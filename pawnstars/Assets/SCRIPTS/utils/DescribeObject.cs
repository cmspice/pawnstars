﻿using UnityEngine;
using System.Collections;

public class DescribeObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var obj = GetComponent<RectTransform>();
        foreach (System.ComponentModel.PropertyDescriptor descriptor in System.ComponentModel.TypeDescriptor.GetProperties(obj))
        {
            string name = descriptor.Name;
            object value = descriptor.GetValue(obj);
            Debug.Log(name + " " +  value);
        }
	}
	
}
