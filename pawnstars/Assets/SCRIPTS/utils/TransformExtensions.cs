﻿using UnityEngine;

static class TransformExtensions
{

    //determines if object is a prefab from project hierarchy or comes from the scene.
    internal static bool IsPrefabGhost(this Transform This)
    {
        var TempObject = new GameObject();
        try
        {
            TempObject.transform.parent = This.parent;

            var OriginalIndex = This.GetSiblingIndex();

            This.SetSiblingIndex(int.MaxValue);
            if (This.GetSiblingIndex() == 0) return true;

            This.SetSiblingIndex(OriginalIndex);
            return false;
        }
        finally
        {
            Object.DestroyImmediate(TempObject);
        }
    }
}