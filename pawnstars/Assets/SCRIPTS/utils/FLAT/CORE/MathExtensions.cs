﻿using UnityEngine;
using System.Collections.Generic;

public static class MathExtensions  {
	
	//Vector stuff
	public static Vector3 component_multiply(this Vector3 aVec, Vector3 scaleVec)
	{
		return new Vector3(aVec.x*scaleVec.x,aVec.y*scaleVec.y,aVec.z*scaleVec.z);
	}
	public static Vector3 component_inverse(this Vector3 aVec)
	{
		return new Vector3(1/aVec.x, 1/aVec.y, 1/aVec.z);
	}
	//returns aVec/scaleVec component wise
	public static Vector3 component_divide(this Vector3 aVec, Vector3 scaleVec)
	{
		return aVec.component_multiply(scaleVec.component_inverse());
	}
	public static Vector3 to_Vector3(this Vector2 aVec, float aZ = 0)
	{
		return new Vector3(aVec.x, aVec.y, aZ);
	}
	//NOTE this function can break with rotations
	public static Vector3 get_global_scale(this Transform aTrans)
	{
		if(aTrans.parent == null)
			return aTrans.localScale;
		return aTrans.localScale.component_multiply(get_global_scale(aTrans.parent));
	}
	//TODO test
	public static void set_global_scale(this Transform aTrans, Vector3 aScale)
	{
		if(aTrans.parent == null)
		{
			aTrans.localScale = aScale;
			return;
		}
		Vector3 parentScale = aTrans.parent.get_global_scale();
		aTrans.localScale = aScale.component_divide(parentScale);
	}

	public static Vector2 project_to_vector2(this Vector3 aVec)
	{
		return new Vector2(aVec.x,aVec.y);
	}
	
	
	//rect stuf
	public static Rect union(this Rect A, Rect B)
	{
		Rect r = A;
		r.xMin = Mathf.Min(A.xMin,B.xMin);
		r.yMin = Mathf.Min(A.yMin,B.yMin);
		r.xMax = Mathf.Max(A.xMax,B.xMax);
		r.yMax = Mathf.Max(A.yMax,B.yMax);
		return r;
	}
	
	//Quaternion stuff
	public static float flat_rotation(this Quaternion aQuat)
	{
		return aQuat.eulerAngles.z;
	}

	public static Quaternion from_flat_rotation(float aRot)
	{
		return Quaternion.AngleAxis(aRot,Vector3.forward);
	}
	

	//lolo repeat
	public static void Shuffle<T>(this IList<T> list)  
	{  
		System.Random rng = new System.Random();  
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = rng.Next(n + 1);  
			T value = list[k];  
			list[k] = list[n];  
			list[n] = value;  
		}  
	}
	
	public static Rect expand(this Rect r, float exp)
	{
		r.x -= 	exp;
		r.y -= exp;
		r.width += 2*exp;
		r.height += 2*exp;
		return r;
	}
	
	//----------
	//bounds nonsense
	//----------
	public static Bounds to_bounds(this Vector3 p)
	{
		Bounds r = new Bounds();
		r.center = p;
		r.extents = new Vector3(0,0,0);
		return r;
	}
	
	public static Bounds union(this Bounds A, Vector3 p)
	{
		Bounds r = new Bounds(A.center,A.size);
		r.Encapsulate(p);	
		return r;
	}
	
	public static Bounds union(this Bounds A, Bounds o)
	{
		Bounds r = new Bounds(A.center,A.size);
		r.Encapsulate(o);
		return r;
	}
	
	
	public static Bounds bounds_from_points(IEnumerable<Vector3> aPoints)
	{
		Bounds? r = null;
		foreach(Vector3 e in aPoints)
		{
			if (r == null)
				r = e.to_bounds();
			else
				r = r.Value.union(e);
		}
		return r.Value;
	}
}
