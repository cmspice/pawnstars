using UnityEngine;
using System.Collections;

public class FlatCameraManager{
    
    public Camera Camera{ get; private set; }

    public Vector3 Center
    {
        get
		{

			return Camera.transform.position + Vector3.forward*Distance;
		}
    }
    public float Distance 
    { 
        get; 
        private set; 
    }
    public bool IsOrthographic
    {
        get { return Camera.orthographic; }
        private set { Camera.orthographic = true; }
    }
    public float Width
    {
        get
        {
            if (IsOrthographic)
                return Camera.orthographicSize * Camera.aspect * 2;
            else
                return Distance * Mathf.Tan(Camera.fieldOfView / 2.0f);
        }
		set{
			Height = value / Camera.aspect;
		}
    }
	
    public float Height
    {
        get { return Width / Camera.aspect; }
		set {
			if (IsOrthographic)
				Camera.orthographicSize = value / 2;
			else //TODO I'm not sure if this is correct or not
				Camera.fieldOfView = Mathf.Atan(Height*Camera.aspect/Distance)*2; 
		}
    }
	
	public Vector2 Size
	{
		get { return new Vector2(Width,Height);}
	}

    
    public FlatCameraManager(Vector3 aCenter, float aDistance)
    {
        Distance = aDistance;
        create_camera(aCenter);
        IsOrthographic = true;
    }
	
	//TODO make sure so camera is not backwards...
    //for setting camera
    void create_camera(Vector3 aCenter)
    {
        Camera = (new GameObject("genFlatCamera")).AddComponent<Camera>();
        Camera.transform.position = aCenter - Vector3.forward * Distance;
        Camera.transform.LookAt(aCenter, Vector3.up);
        Camera.nearClipPlane = 0.1f;
        Camera.farClipPlane = 1000;
        Camera.depth = 99;
        Camera.clearFlags = CameraClearFlags.Depth;
    }
	
	//TODO rename these functions, make them call each other pfftt
	
	public static void fit_camera_to_screen(Camera aCam)
	{
		//comment out this function to disable black bars
		float screenRatio = Screen.width / (float)Screen.height;
		float desiredAspect = screenRatio;
		Rect newRect = aCam.rect;
        if (desiredAspect > screenRatio) //match camera width to screen width
		{
			float yGive = screenRatio/desiredAspect; //desiredHeight to screenHeight
			newRect.y = (1-yGive)/2;
			newRect.height = yGive;
		}
        else
		{
			float xGive = desiredAspect/screenRatio; //screen width to camera width
			newRect.x = (1-xGive)/2;
			newRect.width = xGive;
		}
		aCam.rect = newRect;
		aCam.aspect = desiredAspect;
	}
	
	public void fit_camera_to_screen(bool hard = true)
	{
		float desiredHeight = Screen.height;
		Height = desiredHeight/2f;
		Height = desiredHeight/2f; //don't thin kI need this but just in case...
		//this is for black bars
		if(hard)
			fit_camera_to_screen(Camera);
		Height = desiredHeight/2f;
	}
	
    public void focus_camera_on_element(FlatElementBase aElement)
    {
        Rect focus = aElement.BoundingBox;
             //TODO what if camera is not orthographic
        float texRatio = focus.width / (float)focus.height;
        float camRatio = this.Camera.aspect;
        if (camRatio > texRatio) //match width
            Height = (focus.width / camRatio) / 2.0f;
        else
            Height = (focus.height) / 2.0f;
        Vector3 position = aElement.HardPosition;
        position.z -= Distance;
		Camera.transform.position = position;
    }
	
	public Vector3 screen_pixels_to_camera_pixels(Vector3 aVal)
	{
		return new Vector3(aVal.x * Width/(Camera.rect.width*Screen.width),aVal.y * Height/(Camera.rect.height*Screen.height),aVal.z);
	}

    //other stuff
    //returns world point from coordinates relative to center of screen where screen is (-1,1)x(-1,1)
    public Vector3 get_point(float aX, float aY)
    {
        return Center + new Vector3(aX * Width, aY * Height, 0);
    }

	//this version measures from the entire screen. Not the visible portion
	//TODO does not account for not centered camera, i.e. modify by Camera.rect.x/y
	public Vector3 get_point_total(float aX, float aY)
	{
		return Center + new Vector3(aX * Width * (1/Camera.rect.width), aY * Height * (1/Camera.rect.height), 0);
	}
}
