using UnityEngine;
using System.Collections;

//TODO DELETE???
public class CameraInterpolator 
{
    public Camera Camera
    {
        get;
        private set;
    }
    public float TargetFOV
    {
        get;
        set;
    }
    public float TargetOrthographicHeight
    {
        get;
        set;
    }

    public TRS TargetSpatialPosition
    {
        get;
        set;
    }

    public virtual float SoftInterpolation { get; set; }
    public CameraInterpolator(Camera c)
    {
        this.Camera = c;
        TargetFOV = c.fov;
        TargetOrthographicHeight = c.orthographicSize;
        TargetSpatialPosition = new TRS(c.transform);
        SoftInterpolation = 0.2f;
    }

    public void update(float aDeltaTime)
    {
        TRS nsp = TRS.interpolate_linear(new TRS(Camera.transform), TargetSpatialPosition, SoftInterpolation);
        Camera.transform.position = nsp.t;
        Camera.transform.rotation = nsp.r;

        Camera.orthographicSize = Camera.orthographicSize * (1 - SoftInterpolation) + TargetOrthographicHeight * SoftInterpolation;
        Camera.fov = Camera.fov * (1 - SoftInterpolation) + TargetFOV * SoftInterpolation;
    }


}
