using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class FlatElementMultiBase : FlatElementBase
{
	//TODO should really consider ordering this in increasing depth...
	public List<FlatElementBase> mElements = new List<FlatElementBase>();
	
	//returns null if element did not exist.
	public FlatElementBase reposses_element(FlatElementBase aRemove)
	{
		for(int i = 0; i < mElements.Count; i++)
			if(mElements[i] == aRemove)
		{
			mElements.RemoveAt(i);
			return aRemove;
		}
		return null;
	}
	
	public void destroy_element(FlatElementBase aRemove)
	{
		FlatElementBase d = reposses_element(aRemove);
		if(d != null)
			d.destroy();
	}
	
	protected GameObject create_primary_from_elements()
	{
		if(PrimaryGameObject != null)
			GameObject.Destroy(PrimaryGameObject);
		GameObject r = new GameObject("genMultiFlatParent");
		foreach (FlatElementBase e in mElements)
			e.PrimaryGameObject.transform.parent = r.transform;
		PrimaryGameObject = r;
		return r;
	}
	public override void destroy()
	{
		foreach (FlatElementBase e in mElements)
			e.destroy();
		mElements.Clear();
		try{GameObject.Destroy(PrimaryGameObject);} //probbaly don't need the try catch here
		catch{}
	}


	//NOTE, it is possible to change depth of elements individually after setting Depth. Do so with caution.
	public override int Depth
	{
		get { return mDepth; }
		set
		{
			mDepth = value;
			int depthCounter = mDepth;
			foreach(FlatElementBase e in mElements)
			{
				e.Depth = depthCounter;
				depthCounter += e.DepthUseCount;
			}
		}
	}
	
	public override int DepthUseCount
	{ 
		get{ return (int)mElements.Sum(e=>e.DepthUseCount);}
	}
	
	public override Rect BoundingBox
	{
		get{
			if(mElements.Count == 0)
				return new Rect(0,0,0,0);
			Rect r = mElements[0].BoundingBox;
			foreach(FlatElementBase e in mElements)
				r = r.union(e.BoundingBox);
			return r;
		}
	}

	
	
	bool mEnabled = true;
	public override bool Enabled
	{
		get
		{
			return mEnabled;
		}
		set
		{
			if (mEnabled != value)
			{
				foreach (var e in mElements)
				{
					e.Enabled = value;
				}
				mEnabled = value;
			}
		}
	}
	
	
	
	
	public override void update_parameters(float aDeltaTime)
	{
		base.update_parameters(aDeltaTime);
		foreach (FlatElementBase e in mElements)
			e.update_parameters(aDeltaTime);
	}
	

	
	public override void set_TRS(TRS aTRS)
	{
		base.set_TRS(aTRS);
		foreach (FlatElementBase e in mElements)
			e.set_TRS(e.HardTRS*aTRS);
	}

	public override void set_color(Color aColor)
	{
		base.set_color(aColor);
		foreach (FlatElementBase e in mElements)
			e.set_color(aColor*(e.HardColor)*2); 
	}
}
