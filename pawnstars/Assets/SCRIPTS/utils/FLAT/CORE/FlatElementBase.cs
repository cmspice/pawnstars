using UnityEngine;
using System.Collections.Generic;
using System;

public static class FlatConstants {
    static Color sWhite = new Color(1, 1, 1, 1);
    public static Color White { get { return sWhite / 2; } }
    static Color sTransparentWhite = new Color(1, 1, 1, 0);
    public static Color TransparentWhite { get { return sTransparentWhite / 2; } }
}

public class FlatInterpolationLimits {
	public class InterpolationLimits{
		public float Min {get; set;}
		public float Max {get; set;}
		public float Interp {get; set;}
		public InterpolationLimits()
		{
			Min = 0;
			Max = Mathf.Infinity;
			Interp = .1f;
		}
	}

	public InterpolationLimits T {get; private set;}
	public InterpolationLimits R {get; private set;}
	public InterpolationLimits S {get; private set;}
	public InterpolationLimits C {get; private set;}

	public FlatInterpolationLimits()
	{
		T = new InterpolationLimits();
		R = new InterpolationLimits();
		S = new InterpolationLimits();
		C = new InterpolationLimits();
	}
}
public class FlatElementBase {
	
	public class TimedEventHandler
	{
		Dictionary<QuTimer, Func<FlatElementBase,float,bool>> mTimedEvents = new Dictionary<QuTimer, Func<FlatElementBase,float,bool>>();
		public void update(float aDeltaTime, FlatElementBase aElement)
		{
			LinkedList<KeyValuePair<QuTimer, Func<FlatElementBase,float,bool>>> removal = new LinkedList<KeyValuePair<QuTimer, Func<FlatElementBase,float,bool>>>();
			foreach (KeyValuePair<QuTimer, Func<FlatElementBase,float,bool>> e in mTimedEvents)
			{
				e.Key.update(aDeltaTime);
				if (e.Key.isExpired())
				{
					if (e.Value(aElement,e.Key.getTimeSinceStart()))
						removal.AddLast(e);
				}
			}
			foreach (KeyValuePair<QuTimer, Func<FlatElementBase,float,bool>> e in removal)
				mTimedEvents.Remove(e.Key);
		}
		public void add_event(Func<FlatElementBase,float,bool> aEvent, float aTime)
		{
			mTimedEvents[new QuTimer(0,aTime)] = aEvent;
		}
	}

	Vector3 mPrimaryObjectOriginalScale;
    GameObject mPrimaryGameObject;
    public GameObject PrimaryGameObject
    {
        get
        {
            return mPrimaryGameObject;
        }
        protected set
        {
            mPrimaryGameObject = value;
            //mBaseScale = Vector3.one; //maybe you should this with global scale instead...
			//TODO need global scale
            mPrimaryObjectOriginalScale = mPrimaryGameObject.transform.localScale;
        }
    }

    bool mEnabled = true;
    public virtual bool Enabled
    {
        get
        {
            return mEnabled;
        }
        set
        {
            if (mEnabled != value)
            {
                mEnabled = value;
                foreach (Renderer e in PrimaryGameObject.GetComponentsInChildren<Renderer>())
                    e.enabled = value;
            }
        }
    }

	public TimedEventHandler Events { get; private set; }

	public FlatInterpolationLimits Interp { get; private set;}

	public float TotalInterp{
		set{
			Interp.T.Interp = value;
			Interp.R.Interp = value;
			Interp.S.Interp = value;
			Interp.C.Interp = value;
		}
	}
    

	TRS mBaseHardTRS = TRS.identity;
	TRS mBaseSoftTRS = TRS.identity;
	public virtual TRS SoftTRS{get{return mBaseSoftTRS;} set{mBaseSoftTRS = value;}}
	public virtual TRS HardTRS{get{return mBaseHardTRS;} set{mBaseHardTRS = value;}}

	//just for convenience
	public virtual Vector3 SoftPosition
	{
		get{ return SoftTRS.t; }
		set{ 
			TRS newTRS = SoftTRS;
			newTRS.t = value;
			SoftTRS = newTRS;
		}
	}
	public virtual Vector3 HardPosition
	{
		get { return HardTRS.t; }
		set{ 
			TRS newTRS = HardTRS;
			newTRS.t = value;
			HardTRS = newTRS;
		}
	}
	public virtual Vector3 SoftScale
	{
		get { return SoftTRS.s; }
		set{ 
			TRS newTRS = SoftTRS;
			newTRS.s = value;
			SoftTRS = newTRS;
		}
	}
	public virtual Vector3 HardScale
	{
		get { return HardTRS.s; }
		set{ 
			TRS newTRS = HardTRS;
			newTRS.s = value;
			HardTRS = newTRS;
		}
	}
	public virtual Quaternion SoftRotation
	{	
		get { return SoftTRS.r; }
		set{ 
			TRS newTRS = SoftTRS;
			newTRS.r = value;
			SoftTRS = newTRS;
		}
	}
	public virtual Quaternion HardRotation
	{
		get { return HardTRS.r; }
		set{ 
			TRS newTRS = HardTRS;
			newTRS.r = value;
			HardTRS = newTRS;
		}
	}
	public virtual float SoftFlatRotation
	{
		get { return SoftTRS.r.flat_rotation(); }
		set { SoftRotation = MathExtensions.from_flat_rotation(value); }
	}
	public virtual float HardFlatRotation
	{
		get { return HardTRS.r.flat_rotation(); }
		set { HardRotation = MathExtensions.from_flat_rotation(value); }
	}



	Color mBaseSoftColor;
	Color mBaseHardColor;
	public virtual Color SoftColor
	{
		get { return mBaseHardColor; }
		set { mBaseHardColor = value; }
	}
	public virtual Color HardColor
	{
		get { return mBaseSoftColor; }
		set { mBaseSoftColor = mBaseHardColor = value; }
	}

    public virtual Rect BoundingBox
    {
        get { return new Rect(0, 0, 0, 0); }
    }
	
    protected int mDepth = 0;
    public virtual int Depth
    {
        get { return mDepth; }
        set
        {
            mDepth = value;
            foreach (Renderer e in PrimaryGameObject.GetComponentsInChildren<Renderer>())
                e.material.renderQueue = mDepth;
        }
    }
	public virtual int DepthUseCount
	{ 
		get{ return 0;}
	}
	
	
    public virtual Shader HardShader
    {
		set
		{
			if (PrimaryGameObject != null)
	        {
	            foreach (Renderer e in PrimaryGameObject.GetComponentsInChildren<Renderer>())
				{
	            	e.material.shader = value;
					e.material.renderQueue = mDepth;
				}
	        }
		}
    }
	
    public virtual void destroy()
    {
		//obviously, don't call this if you want to handle destruction yourself
		GameObject.Destroy(PrimaryGameObject);
    }

    public FlatElementBase()
    {
		Interp = new FlatInterpolationLimits();
		Interp.C.Max = 1f;
		Interp.C.Min = .04f;


        HardScale = Vector3.one;
		HardColor = new Color(0.5f,0.5f,0.5f,0.5f);
		
        Events = new TimedEventHandler();
    }

    public virtual void set_position(Vector3 aPos)
    {
        if (PrimaryGameObject != null)
        {
            PrimaryGameObject.transform.position = aPos;
        }
    }
    public virtual void set_scale(Vector3 aScale)
    {
        if (PrimaryGameObject != null)
        {
            if(PrimaryGameObject.transform.localScale != aScale)
                PrimaryGameObject.transform.localScale = aScale;
        }
    }
    public virtual void set_rotation(Quaternion aRot)
    {
        if (PrimaryGameObject != null)
        {
            PrimaryGameObject.transform.rotation = aRot;
        }
    }

	public virtual void set_TRS(TRS aTRS)
	{
		if(PrimaryGameObject != null)
		{
			PrimaryGameObject.transform.position = aTRS.t;
			PrimaryGameObject.transform.rotation = aTRS.r;
			PrimaryGameObject.transform.set_global_scale(mPrimaryObjectOriginalScale.component_multiply(aTRS.s)); 
		}
	}

    public virtual void set_color(Color aColor)
    {
        if (PrimaryGameObject != null)
        {

            foreach (MeshRenderer e in PrimaryGameObject.GetComponentsInChildren<MeshRenderer>())
            {
				TextMesh tMesh = e.gameObject.GetComponent<TextMesh>();
				if(tMesh == null)
				{
	                try { e.GetComponent<Renderer>().material.SetColor("_TintColor", aColor); }
	                catch { }
	                try { 
						e.GetComponent<Renderer>().material.color = aColor; }
	                catch { }
				}

            }
            /*
            Renderer rend = PrimaryGameObject.GetComponentInChildren<Renderer>();
            if (rend != null)
            {
                try { rend.material.SetColor("_TintColor", aColor); }
                catch { }
                try { rend.material.color = aColor; }
                catch { }
            }*/
        }
    }
    
	public virtual float get_alpha()
	{
		if(PrimaryGameObject != null)
			foreach(Renderer e in PrimaryGameObject.GetComponentsInChildren<Renderer>())
			{
				try{ return e.material.color.a; break;}
				catch { }
			}
		return 0;
	}

	public void update(float aDeltaTime)
	{
		update_parameters(aDeltaTime);
		set();
	}

	public virtual void update_parameters(float aDeltaTime)
	{
		Events.update(aDeltaTime, this);

		HardRotation = Quaternion.Slerp(HardRotation, SoftRotation, Interp.R.Interp);
		HardScale = Vector3.Lerp(HardScale,SoftScale,Interp.S.Interp);

		//rotation
		/*
		if(Interpolation.R.Max < Mathf.Infinity || Interpolation.R.Min > 0)
		{
			float minLimitChange = Interpolation.R.Min * aDeltaTime;
			float maxLimitChange = Interpolation.R.Max * aDeltaTime;
			
			Quaternion desiredRot = (1 - Interpolation.R.Interpolation) * HardRotation + Interpolation.R.Interpolation * SoftRotation;

			//TODO
		}
		else
			HardPosition = (1 - SoftInterpolation) * HardPosition + SoftInterpolation * SoftPosition;

		if(Interpolation.S.Max < Mathf.Infinity || Interpolation.S.Min > 0)
		{
			float minLimitChange = Interpolation.S.Min * aDeltaTime;
			float maxLimitChange = Interpolation.S.Max * aDeltaTime;
			
			Vector3 desiredScale = (1 - Interpolation.S.Interpolation) * HardScale + Interpolation.S.Interpolation * SoftScale;
			float desiredScaleDistance = (desiredScale-HardPosition).magnitude;
			if((HardScale-SoftScale).magnitude < minLimitChange)
				HardScale = desiredScale;
			else if(desiredScaleDistance > 0)
				HardScale += (desiredScale-HardScale) / desiredScaleDistance * Mathf.Clamp(desiredScaleDistance,minLimitChange,maxLimitChange);
		}
		else
			HardScale = (1 - Interpolation.S.Interpolation) * HardScale + Interpolation.S.Interpolation * SoftScale;
		*/
		
		if(Interp.T.Max < Mathf.Infinity || Interp.T.Min > 0)
		{
			float minLimitChange = Interp.T.Min * aDeltaTime;
			float maxLimitChange = Interp.T.Max * aDeltaTime;
			
			Vector3 desiredPosition = (1 - Interp.T.Interp) * HardPosition + Interp.T.Interp * SoftPosition;
			float desiredPositionDistance = (desiredPosition-HardPosition).magnitude;
			if((HardPosition-SoftPosition).magnitude < minLimitChange)
				HardPosition = desiredPosition;
			else if(desiredPositionDistance > 0)
				HardPosition += (desiredPosition-HardPosition) / desiredPositionDistance * Mathf.Clamp(desiredPositionDistance,minLimitChange,maxLimitChange);
		}
		else
			HardPosition = (1 - Interp.T.Interp) * HardPosition + Interp.T.Interp * SoftPosition;
		
		
		Vector4 lerpingColorA = new Vector4(mBaseSoftColor.r, mBaseSoftColor.g, mBaseSoftColor.b, mBaseSoftColor.a);
		Color targetColor = (1 - Interp.C.Interp) * mBaseSoftColor + Interp.C.Interp * mBaseHardColor;
		Vector4 lerpingColorB = new Vector4(targetColor.r, targetColor.g, targetColor.b, targetColor.a);
		Vector4 finalColor = new Vector4(mBaseHardColor.r, mBaseHardColor.g, mBaseHardColor.b, mBaseHardColor.a);
		Vector4 lerpingColorDir = lerpingColorB - lerpingColorA;
		float lerpingColorDist = lerpingColorDir.magnitude;
		Vector4 actualColor = lerpingColorA;
		
		float maxChange = Interp.C.Max*aDeltaTime;
		float minChange = Interp.C.Min*aDeltaTime;
		
		if((lerpingColorA - finalColor).magnitude < minChange)
			actualColor = finalColor;
		else if(lerpingColorDist > 0)
			actualColor = lerpingColorA + lerpingColorDir/lerpingColorDist * Mathf.Clamp(lerpingColorDist,minChange,maxChange);
		
		mBaseSoftColor = new Color(actualColor.x,actualColor.y,actualColor.z,actualColor.w);
	}
	
	public virtual void set()
	{
		set_TRS(HardTRS);
		set_color(mBaseSoftColor);
	}

}
