﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;

public enum GameMode { NONE, STORE, TRAVEL, SHOP };

public class MAIN : MonoBehaviour {
    public static MAIN sInst { get; private set; }
    public Shader reduceColorShader;
    public Shader cutsceneShader;

    public GameMode GameMode { get; private set; }
    public PokedexManager PokedexManager { get; private set; }
    public ShopManager ShopManager { get; private set; }
    public MapManager MapManager { get; private set; }
    public CutsceneManager CutsceneManager { get; private set; }
    public InventoryManager InventoryManager { get; private set; }


    float mCash;
    public float CASH
    {
        get { return mCash; }
        set
        {
            mCash = value;
            if(MapManager != null)
                MapManager.SetCASH();

        }
    }

    public static bool ShowCursor
    {
        set
        {
            if (value)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }
    }

    void Awake()
    {
        sInst = this;
    }

	void Start () {
        GameMode = GameMode.NONE;

        CASH = 1300;
       
        //var item = new ItemInternal();
        //item.prefabName = "test";
        //item.SetupItem();

        CutsceneManager = gameObject.AddComponent<CutsceneManager>();
        CutsceneManager.Initialize();

        ShopManager = gameObject.AddComponent<ShopManager>();
        ShopManager.OnShopLoaded += ShopLoadedListener;
        
        PokedexManager = gameObject.AddComponent<PokedexManager>();
        PokedexManager.Initialize();
		PokedexManager.ShowPokedex = false;

        MapManager = gameObject.AddComponent<MapManager>();
        MapManager.Initialize();
        MapManager.TravelEvent += TravelEventListener;

        InventoryManager = gameObject.AddComponent<InventoryManager>();
        InventoryManager.Initialize();

        //ShopManager.LoadShop("SHOP - PLAYER");
        TravelEventListener(MapLocationList.sHome);
        //LoadCutscene("START");
	}

    void LoadCutscene(string aSceneName)
    {
        CutsceneManager.LoadCutscene(aSceneName);
    }

    void TravelEventListener(MapLocation aLoc)
    {
        //Debug.Log(aLoc.sceneName);

        if (aLoc.sceneName == ShopManager.CurrentShop && ShopManager.CurrentShop != null)
            return;

        MapManager.SetCurrentLocation(aLoc.title);

        if (ShopManager.IsPlayerShop)
        {
            Debug.Log("serializing scene");
            ShopSerializer.SerializeSceneToFile();
        }

        InventoryManager.DestroyAllSceneItems();
        ShopManager.UnloadCurrentScene();

        if (aLoc.cutsceneName == "")
            LoadCutscene("default");
        else if(aLoc.cutsceneName != "")
            LoadCutscene(aLoc.cutsceneName);

        if (aLoc.sceneName != "")
            CutsceneManager.NextScene = aLoc.sceneName;

        MapManager.ShowMap = false;
    }


    void ShopLoadedListener()
    {
        if (ShopManager.IsPlayerShop)
        {

            //no more scene loading in the current version of the game
            //plus this is kind of broken when it comes to physics stuff
            if (false && ShopSerializer.DoesSaveExist())
            {
                InventoryManager.AddNewSceneItems();
                InventoryManager.DestroyAllSceneItems();
                ShopSerializer.DeserializeSceneFromFile();
            }

            if (InventoryManager.HasTedNugentPinballMachine)
                CutsceneManager.Error("You made it back in one piece with your Ted Nugent Pinball Machine. Now place it in your house. That's all folks.");
            else
                CutsceneManager.Error("Meet b9nvb-5232912585@sale.craigslist.org out at the Gila County swap meet to pick up your Ted Nugent Pinball Machine");
        }
        InventoryManager.AddNewSceneItems();
    }

    public void ToggleMap()
    {
        MapManager.ShowMap = !MapManager.ShowMap;
        ShowCursor = MapManager.ShowMap;
    }

	void Update () {
        if(Camera.main != null)
            ScanItems(Camera.main);

        //for testing, make sure to do this only after the scene loads.
        //if (Input.GetKeyDown(KeyCode.R))
            //ShopSerializer.TestSerialize();

        if (Input.GetKeyDown(KeyCode.B))
        {
            float cost = PokedexManager.CurrentItemCost;
            if(cost != -1)
            {
                if (cost <= CASH)
                {
                    if (PokedexManager.IsTedNugentPinballMachine) 
                        CutsceneManager.Error(CutsceneDump.sNugent,false);
                    CASH -= cost;
                    TakeItem(Camera.main);
                }
                else
                {
                    CutsceneManager.Error("!!ERROR!!\nNOT ENOUGH MONEY");
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            //TODO be nice to only let player do this in house?? lol
            InventoryManager.DumpItem(Camera.main);
        }

        if (Input.GetKeyDown(KeyCode.M))
            ToggleMap();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (ShopManager.IsPlayerShop)
            {
                Debug.Log("serializing scene");
                ShopSerializer.SerializeSceneToFile();
            }
            Application.Quit();
        }

        var keys = new KeyCode[] { KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9 };
        for(int i = 0; i < keys.Length; i++)
        {
            var e = keys[i];
            if (Input.GetKeyDown(e))
            {
                if (i < MapLocationList.sLocations.Count)
                    TravelEventListener(MapLocationList.sLocations.ToArray()[i].Value);
            }
        }
	}

    ItemBehaviour RaycastItemBehaviour(Camera aCam)
    {
        var ray = aCam.ViewportPointToRay(new Vector3(.5f, .5f, 0));
        RaycastHit rhit;
        if (Physics.Raycast(ray, out rhit, 1000))
            if (rhit.rigidbody != null)
                return rhit.rigidbody.GetComponent<ItemBehaviour>();
        return null;
    }

    //TODO move somewhere else
    void ScanItems(Camera aCam) 
    {
        var item = RaycastItemBehaviour(aCam);
        if (item != null)
        {
            PokedexManager.ShowItem(item.ItemInternal);
            ShopManager.FPC.Crosshair = true;
        }
        else
        {
            PokedexManager.ClearItem();
            ShopManager.FPC.Crosshair = false;
        }
    }

    void TakeItem(Camera aCam)
    {
        var item = RaycastItemBehaviour(aCam);
        if (item != null)
            InventoryManager.TakeItem(item.ItemInternal);
    }
}
