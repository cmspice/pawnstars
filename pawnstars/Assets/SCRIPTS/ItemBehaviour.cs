﻿using UnityEngine;
using System.Collections;

public class ItemBehaviour : MonoBehaviour
{
	public string ItemName = "GENERIC";

	public enum State{stationary, held, falling};
	public State state = State.stationary;
	Rigidbody mRigidbody;
    public Vector3 HeldPosition;
    public float price;
    public ItemInternal ItemInternal { get; set; }


	public void Initialize()
	{
		mRigidbody = gameObject.AddComponent<Rigidbody> ();
	}
	void Update()
	{

	}

	void FixedUpdate()
	{
		switch (state)
		{
		case State.stationary:
			break;
		case State.held:
			UpdateHeld();
			break;
		case State.falling:
			UpdateFalling();
			break;

		}
	}

	void UpdateHeld()
	{
		fallTimer = 0;
		mRigidbody.MovePosition (HeldPosition);
	}

	float fallTimer = 0;
	void UpdateFalling()
	{
		if (mRigidbody.velocity.magnitude == 0)
		{
			fallTimer += Time.fixedDeltaTime;
			if (fallTimer > 0.05f)
			{
				BeginStationary ();
			}
		} else
			fallTimer = 0;
	}

	public void BeginStationary()
	{
		state = State.stationary;
		mRigidbody.isKinematic = true;
		gameObject.layer = LayerMask.NameToLayer ("Item");
	}

	public void BeginFalling()
	{
		gameObject.layer = LayerMask.NameToLayer ("HeldItem");

		mRigidbody.isKinematic = false;
		mRigidbody.useGravity = true;
		state = State.falling;
	}

	public void BeginHeld()
	{
		gameObject.layer = LayerMask.NameToLayer ("HeldItem");
		mRigidbody.isKinematic = false;
		mRigidbody.useGravity = false;
		state = State.held;
	}
}
