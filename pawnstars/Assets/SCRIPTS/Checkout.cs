﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Checkout : MonoBehaviour
{
	public Text text;
	Transform virtualInventory;

	ItemBehaviour curItem;
	// Use this for initialization
	void Start ()
	{
		GameObject virtualInvObject = GameObject.Find ("INVENTORY");

		if(virtualInvObject != null)
			virtualInventory = virtualInvObject.transform;

		if (virtualInventory == null)
			virtualInventory = new GameObject ("INVENTORY").transform;

		virtualInventory.position = Vector3.down * 2000;

		text.text = "$0.00";
	}
	bool routineStarted = false;
	// Update is called once per frame
	void Update () 
	{
		if (curItem != null) {
			if (curItem.state == ItemBehaviour.State.stationary && !routineStarted) {
				StartCoroutine (checkoutRoutine ());
				routineStarted = true;
			}
		}
		else 
		{
			text.text = "$00.00";
		}
	}

	IEnumerator checkoutRoutine()
	{
		text.text = curItem.price.ToString ("$00.00");
		yield return new WaitForSeconds (1);
		curItem.transform.parent = virtualInventory;
		curItem.transform.localPosition = Vector3.zero;


	}

	void OnTriggerEnter(Collider other)
	{
		Debug.Log ("trigger enter");

		ItemBehaviour item = other.attachedRigidbody.GetComponent<ItemBehaviour> ();
		if (item == null) 
			return;

		
		Debug.Log ("trigger got item");
		curItem = item;

	}

	void OnTriggerExit(Collider other)
	{
		curItem = null;
		routineStarted = false;
		StopAllCoroutines ();
	}
}
