﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PokedexManager : MonoBehaviour {
    RenderTexture itemRT;
    Camera itemCamera;
    GameObject canvas;
    RawImage itemImage;
    Text titleText, priceText, descriptionText;

    GameObject currentDisplayItemTempGameObject;
    ItemInternal currentDisplayItem;

    Camera displayCamera;

    public float CurrentItemCost
    {
        get
        {
            if (currentDisplayItem == null)
                return -1;
            return currentDisplayItem.BaseValue;
        }
    }

    public bool IsTedNugentPinballMachine
    {
        get
        {
            if (currentDisplayItem == null)
                return false;
            return currentDisplayItem.prefabName == "ITEM_PINBALL_NUGENT";

        }
    }

    public bool ShowPokedex
    {
        set
        {
            canvas.SetActive(value);
        }
    }
    public void Initialize()
    {

        itemRT = new RenderTexture(1024, 1024, 32); 
        itemCamera = new GameObject("itemRTCamera").AddComponent<Camera>();
        itemCamera.nearClipPlane = 0.001f;
        itemCamera.backgroundColor = Color.clear;
        itemCamera.clearFlags = CameraClearFlags.SolidColor;
        itemCamera.targetTexture = itemRT;
        itemCamera.transform.position = new Vector3(10000, 0, 0);

        displayCamera = new GameObject("itemDisplayCamera").AddComponent<Camera>();
        displayCamera.backgroundColor = Color.clear;
        displayCamera.clearFlags = CameraClearFlags.Depth;
        displayCamera.transform.position = new Vector3(-10000, 0, 0);
        displayCamera.depth = 8;


        canvas = GameObject.Instantiate(Resources.Load("SYSTEM_PREFABS/PokedexFlatUI")) as GameObject;
        titleText = canvas.transform.GetChild(0).FindChild("title").gameObject.GetComponent<Text>();
        priceText = canvas.transform.GetChild(0).FindChild("price").gameObject.GetComponent<Text>();
        descriptionText = canvas.transform.GetChild(0).FindChild("description").gameObject.GetComponent<Text>();
        itemImage = canvas.transform.GetChild(0).FindChild("item").gameObject.GetComponent<RawImage>();
        itemImage.texture = itemRT;

        itemCamera.aspect = itemImage.GetComponent<RectTransform>().GetWidth() / itemImage.GetComponent<RectTransform>().GetHeight(); //TODO is this correct??

        ClearItem();

        canvas.transform.position = displayCamera.ScreenToWorldPoint(new Vector3(displayCamera.pixelWidth - 300, 220, 7));
        //canvas.transform.LookAt(displayCamera.transform);
        canvas.transform.Rotate(Vector3.up, 20,Space.Self);
    }

    public void Update() 
    {
        if (currentDisplayItemTempGameObject != null)
            currentDisplayItemTempGameObject.transform.Rotate(Vector3.up, 10 * Time.deltaTime, Space.World);
    }

    public void DestroyCurrentDisplayItem()
    {
        if(currentDisplayItemTempGameObject != null)
            GameObject.Destroy(currentDisplayItemTempGameObject);
        currentDisplayItem = null;
    }

    public void ClearItem()
    {
        DestroyCurrentDisplayItem();
        titleText.text = "";
        descriptionText.text = "SCAN ITEM";
        priceText.text = "";
            

    }

    //TODO move this and make it an extension method
    public static Bounds CompoundColliderBounds(GameObject parent)
    {
        Collider[] myColliders = parent.GetComponentsInChildren<Collider>();
        Bounds r = new Bounds(parent.transform.position, Vector3.zero);
        foreach (Collider nextCollider in myColliders)
        {
            r.Encapsulate(nextCollider.bounds);
        }
        return r;
    }

    public void ShowItem(ItemInternal aItem)
    {
        if (aItem == currentDisplayItem)
            return;

        DestroyCurrentDisplayItem();
        currentDisplayItemTempGameObject = GameObject.Instantiate(aItem.Go) as GameObject;
        var size = CompoundColliderBounds(currentDisplayItemTempGameObject);
        currentDisplayItemTempGameObject.transform.position = itemCamera.transform.position + itemCamera.transform.forward*(0.05f + size.size.magnitude*1.1f);
        currentDisplayItemTempGameObject.transform.Rotate(new Vector3(1, .2f, 0), 20);
        GameObject.DestroyImmediate(currentDisplayItemTempGameObject.GetComponent<ItemBehaviour>()); //for pokedex display purposes only, so we don't want ItemBehaviour
        GameObject.DestroyImmediate(currentDisplayItemTempGameObject.GetComponent<Rigidbody>());

        titleText.text = aItem.Name;
        descriptionText.text = aItem.Description;
        priceText.text = aItem.BaseValue.ToString("C", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
        currentDisplayItem = aItem;

        //Debug.Log("Set display item to " + aItem.Name);
        
    }
}

