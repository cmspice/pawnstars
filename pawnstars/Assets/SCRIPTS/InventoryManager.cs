﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class InventoryManager : MonoBehaviour
{
    HashSet<ItemInternal> sceneItems = new HashSet<ItemInternal>();
    HashSet<ItemInternal> inventory = new HashSet<ItemInternal>();

    public bool HasTedNugentPinballMachine
    {
        get
        {
            return inventory.Where(e => e.prefabName == "ITEM_PINBALL_NUGENT").Count() > 0;
        }
    }

    public void Initialize()
    {

    }

    public void TakeItem(ItemInternal aItem)
    {
        aItem.DestroyItem();
        inventory.Add(aItem);
    }

    public void DumpItem(Camera aCam)
    {
        if (inventory.Count == 0)
            return;
        var position = aCam.ViewportToWorldPoint(new Vector3(.5f, .5f, 1));
        var item = inventory.First();
        inventory.Remove(item);
        item.BuildItem(true);
        item.Go.transform.position = position;
        AddItemToScene(item);
    }


    public void AddItemToScene(ItemInternal aItem)
    {
        sceneItems.Add(aItem);
    }

    public void AddNewSceneItems()
    {
        int count = 0;
        var items = GameObject.FindObjectsOfType<ItemBehaviour>().Where(e => e.gameObject.activeInHierarchy).Select(e => e.ItemInternal);
        foreach(var e in items)
        {
            if (!sceneItems.Contains(e)) //probably don't actually need to do this
            {
                sceneItems.Add(e);
                count++;
            }
        }
        Debug.Log("added " + count + " new items from scene");
    }

    public void DestroyAllSceneItems()
    {
        foreach (var e in sceneItems)
        {
            e.DestroyItem();
        }
        sceneItems.Clear();
    }

    //TODO don't really need this anymore, should delete
    //call this immediately after destroying a shop
    public void CleanSceneItems() 
    {
        List<ItemInternal> removal = new List<ItemInternal>();
        foreach (var e in sceneItems)
        {
            if (e.Go == null || !e.Go.activeInHierarchy)
            {
                removal.Add(e);
            }
        }
        foreach (var e in removal)
            sceneItems.Remove(e);
        Debug.Log("cleaned out " + removal.Count + " items");
    }

}
