﻿using UnityEngine;
using System.Collections;

public class ItemPrefabKnife : ItemPrefabBase {
	public MeshData[] Blades;
	public MeshData[] Handles;
	static Texture2D[] BladeTextures;
	static Texture2D[] HandleTextures;

	public override GameObject Build(ItemInternal aItem) 
	{
		//Debug.Log ("YO IM BUILDIN A KNIFE");

		aItem.Name = displayName;
		aItem.Description = description;
		aItem.BaseValue = baseValue;

		ManageSeed (aItem);
		LoadResources ();
		GameObject r;
		
		//can we make this work as a build??
		if (gameObject.transform.IsPrefabGhost())
			r = GameObject.Instantiate(gameObject) as GameObject;
		else r = gameObject;

		GameObject BladeObject = r.transform.FindChild ("BLADE").gameObject;
		GameObject HandleObject = r.transform.FindChild ("HANDLE").gameObject;

		MeshData Blade = Blades [Random.Range (0, Blades.Length)];
		MeshData Handle = Handles [Random.Range (0, Handles.Length)];

		//Debug.Log ("HANDLE: " + Handle.mesh.name);
		BladeObject.GetComponent<MeshFilter> ().mesh = Blade.mesh;
		HandleObject.GetComponent<MeshFilter> ().mesh = Handle.mesh;

		BladeObject.GetComponent<MeshRenderer> ().material.mainTexture = BladeTextures [Random.Range (0, BladeTextures.Length)];
		HandleObject.GetComponent<MeshRenderer> ().material.mainTexture = HandleTextures [Random.Range (0, HandleTextures.Length)];



		string lengthAdjective = "";
		switch (Random.Range (0, 4)) 
		{
		case 0:
			lengthAdjective = "short";
			BladeObject.transform.localScale -= Vector3.up * 0.2f;
				break;
		case 1:
			lengthAdjective = "medium";
				break;
		case 2:
			lengthAdjective = "long";
			BladeObject.transform.localScale += Vector3.up * 0.4f;
			break;
		case 3:
			lengthAdjective = "wide";
			BladeObject.transform.localScale += Vector3.forward * Random.Range(0.2f,0.4f);
			break;
		}

		switch (Random.Range (0, 3))
		{
		case 0:
			aItem.Name = Blade.GetRandomAdjective() + " Knife";
			break;
		case 1:
			aItem.Name = Handle.GetRandomAdjective() + " Knife";
			break;
		case 2:
			aItem.Name = Handle.GetRandomAdjective() + " " + Blade.GetRandomAdjective() + " Knife";
			break;
		}

		aItem.Description = "A " + aItem.Name.ToLower () + " with a " + lengthAdjective + " length blade. " + Blade.GetRandomDescription () + " " + Handle.GetRandomDescription ();

		GameObject.Destroy(r.GetComponent<ItemPrefabBase>());
		
		aItem.Go = r;
		return r;
	}



	
	void LoadResources()
	{
		if (BladeTextures != null)
			return;
		BladeTextures = Resources.LoadAll<Texture2D> ("TEXTURES/KNIVES/BLADES");
		HandleTextures = Resources.LoadAll<Texture2D> ("TEXTURES/KNIVES/HANDLES");
	}
}