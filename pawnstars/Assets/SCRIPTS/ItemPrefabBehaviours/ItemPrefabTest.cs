﻿using UnityEngine;
using System.Collections;

public class ItemPrefabTest : ItemPrefabBase
{
    public override GameObject Build(ItemInternal aItem)
    {
        aItem.Name = displayName;
        aItem.Description = description;
        aItem.BaseValue = baseValue;
        aItem.SpecialBehaviour += () => Debug.Log("test");
        var r = GameObject.CreatePrimitive(PrimitiveType.Cube);
        r.transform.position = transform.position;
        aItem.Go = r;
        if(!transform.IsPrefabGhost())
            GameObject.Destroy(gameObject);
        return r;
    }
}