﻿using UnityEngine;
using System.Collections;

public class ItemPrefabMug : ItemPrefabBase {
	MeshFilter meshFilter;
	public Mesh[] MugMeshes;
	static Texture2D[] ShiftableTextures;
	static Texture2D[] UnshiftableTextures;
	static TextAsset[] Descriptions;

	public Shader HSVShiftShader;

	public override GameObject Build(ItemInternal aItem)
	{
        GameObject r;
        if (gameObject.transform.IsPrefabGhost())
            r = GameObject.Instantiate(gameObject) as GameObject;
        else r = gameObject;

		LoadResources ();

		//manage the seed
		ManageSeed (aItem);

		bool UseShiftable = Random.Range (0f, 1f) > 0.25f;
		int meshIndex = Random.Range (0, MugMeshes.Length);
		int textureIndex = UseShiftable ? Random.Range (0, ShiftableTextures.Length) : Random.Range (0, UnshiftableTextures.Length);

		//set the mesh
		MeshFilter meshFilter = r.GetComponentInChildren<MeshFilter> ();
		meshFilter.mesh = MugMeshes [meshIndex];

		//set the texture
		Material mat = r.GetComponentInChildren<MeshRenderer> ().material;
		mat.mainTexture = UseShiftable ? ShiftableTextures [textureIndex] : UnshiftableTextures [textureIndex];

		//set colors if shiftable tex
		if (UseShiftable) 
		{
			mat.shader = HSVShiftShader;
			float hue = Random.Range(0f, 360f);
			//Debug.Log("HUE: " + hue);
			mat.SetFloat("_HueShift",hue);
			mat.SetFloat ("_SatShift", Random.Range(0.75f, 1.3f));
			mat.SetFloat("_ValShift", Random.Range (0.8f, 1.2f));
		}

		string itemName = mat.mainTexture.name;
		itemName = itemName.Split ('_') [1] + " Mug";
		aItem.Name = itemName;
		aItem.Description = Descriptions[Random.Range(0,Descriptions.Length)].text;
		aItem.BaseValue = baseValue;


	//	aItem.SpecialBehaviour += () => Debug.Log("test");
		r.transform.position = transform.position;
		aItem.Go = r;
		
		GameObject.Destroy(r.GetComponent<ItemPrefabBase>());

		return r;
	}

	void LoadResources()
	{
		if (ShiftableTextures != null)
			return;
		ShiftableTextures = Resources.LoadAll<Texture2D> ("TEXTURES/MUGS/HSVSHIFTABLE");
		UnshiftableTextures = Resources.LoadAll<Texture2D> ("TEXTURES/MUGS/NONSHIFTABLE");
		Descriptions = Resources.LoadAll<TextAsset> ("GENERATIVE DESCRIPTIONS/MUGS");
	}

}
