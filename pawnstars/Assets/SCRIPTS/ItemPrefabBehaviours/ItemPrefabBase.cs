﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;


//itemprefab -> iteminternal -> gameobject
//itemprefab -> iteminternal -> serialized -> iteminternal
    

public class ItemPrefabBase : MonoBehaviour
{
	//haha//
	//yeaaa//

    public string displayName;
    public string description;
    public float baseValue;
    public bool fixedPosition;

    public virtual GameObject Build(ItemInternal aItem) 
    {
        aItem.Name = displayName;
        aItem.Description = description;
        aItem.BaseValue = baseValue;
		//Debug.Log (displayName);
        GameObject r;

        //can we make this work as a build??
        if (gameObject.transform.IsPrefabGhost())
            r = GameObject.Instantiate(gameObject) as GameObject;
        else r = gameObject;
		
        GameObject.Destroy(r.GetComponent<ItemPrefabBase>());

        aItem.Go = r;
        return r;
    }

	protected void ManageSeed(ItemInternal aItem)
	{
		//manage the seed
		if (aItem.generationSeed == -1) 
		{
			//make a new seed!
			aItem.generationSeed = (int)(Random.value * 10000f);
			Random.seed = aItem.generationSeed;
		} 
		else 
		{
			//use the existing seed
			Random.seed = aItem.generationSeed;
		}
	}

	[System.Serializable]
	public class MeshData
	{
		public Mesh mesh;
		public string[] Adjective;
		public string[] Description;
		
		public string GetRandomAdjective()
		{
			if (Adjective.Length == 0)
				return "";
			return Adjective [Random.Range (0, Adjective.Length)];
		}
		public string GetRandomDescription()
		{
			if (Description.Length == 0)
				return "";
			return Description [Random.Range (0, Description.Length)];
		}
	}
}
