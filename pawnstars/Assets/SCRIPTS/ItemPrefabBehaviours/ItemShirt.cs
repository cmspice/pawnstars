﻿using UnityEngine;
using System.Collections;

public class ItemShirt : ItemPrefabBase
{
    //TODO hangers
    //TODO decal

    MeshFilter meshFilter;
    static Texture2D[] ShiftableTextures;
    static Texture2D[] UnshiftableTextures;

    public Shader HSVShiftShader;

    public override GameObject Build(ItemInternal aItem)
    {
        GameObject r;
        if (gameObject.transform.IsPrefabGhost())
            r = GameObject.Instantiate(gameObject) as GameObject;
        else r = gameObject;


        LoadResources();

        //manage the seed
        ManageSeed(aItem);

        bool UseShiftable = Random.Range(0f, 1f) > 0.25f;
        int textureIndex = UseShiftable ? Random.Range(0, ShiftableTextures.Length) : Random.Range(0, UnshiftableTextures.Length);

        GameObject front = GameObject.CreatePrimitive(PrimitiveType.Quad);
        GameObject back = GameObject.CreatePrimitive(PrimitiveType.Quad);

        var tex = UseShiftable ? ShiftableTextures[textureIndex] : UnshiftableTextures[textureIndex];

        float hue = Random.Range(0f, 360f);
        float sat = Random.Range(0.75f, 1.3f);
        float val = Random.Range(0.8f, 1.2f);

        foreach (var e in new GameObject[] { front, back })
        {
            e.transform.localScale = Vector3.one * .7f;
            e.transform.parent = r.transform;
            e.transform.localPosition = new Vector3(0, -0.08f, 0);
            e.transform.localRotation = Quaternion.identity;
            //set the texture
            Material mat = e.GetComponent<Renderer>().material;
            mat.mainTexture = tex;
            mat.shader = HSVShiftShader;

            //set colors if shiftable tex
            if (UseShiftable)
            {
                
                //Debug.Log("HUE: " + hue);
                mat.SetFloat("_HueShift", hue);
                mat.SetFloat("_SatShift", sat);
                mat.SetFloat("_ValShift", val);
            }
        }
        back.transform.Rotate(Vector3.up, 180);

        var bc = r.AddComponent<BoxCollider>();
        bc.center = new Vector3(0, -.08f, 0);
        bc.size = new Vector3(.39f, .55f, .03f);
        
        string itemName = tex.name;
        itemName = "t-shirt";// itemName.Split('_')[1] + " t-shirt";
        aItem.Name = itemName;
        aItem.Description = "It's a t-shirt!";
        aItem.BaseValue = baseValue;

        //	aItem.SpecialBehaviour += () => Debug.Log("test");
        r.transform.position = transform.position;
        aItem.Go = r;

        GameObject.Destroy(r.GetComponent<ItemPrefabBase>());

        return r;
    }

    void LoadResources()
    {
        if (ShiftableTextures != null)
            return;
        ShiftableTextures = Resources.LoadAll<Texture2D>("TEXTURES/TSHIRTS");
        UnshiftableTextures = Resources.LoadAll<Texture2D>("TEXTURES/TSHIRTS");
    }

}
