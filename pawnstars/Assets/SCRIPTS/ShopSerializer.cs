﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
public class ShopSerializer 
{


    public static void SerializeSceneToFile()
    {
        using (StreamWriter writer = new StreamWriter("save.txt"))
        {
            writer.Write(SerializeScene());
        }
        Debug.Log("wrote save.txt");
    }

    public static bool DoesSaveExist()
    {
        return File.Exists("save.txt");
    }

    public static void DeserializeSceneFromFile()
    {
        using (StreamReader reader = new StreamReader("save.txt"))
        {
            DeserializeScene(reader.ReadToEnd());
        }
        Debug.Log("read save.txt");
    }

    public static string SerializeScene()
    {
        //TODO there should be a List<ItemInternal> somewhere at some point
        var items = GameObject.FindObjectsOfType<ItemBehaviour>().Where(e => e.gameObject.activeInHierarchy).Select(e=>e.ItemInternal);

        List<SerializeItemData> ser = new List<SerializeItemData>();
        foreach (var e in items)
        {
            SerializeItemData add = new SerializeItemData();
            add.prefabName = e.prefabName;
            add.generationSeed = e.generationSeed;
            add.position = e.Go.transform.position;
            add.scale = e.Go.transform.localScale;
            add.rotation = e.Go.transform.rotation;
            ser.Add(add);
        }
        return JsonConvert.SerializeObject(ser);
    }

    public static void DeserializeScene(string aJsonScene)
    {
        List<SerializeItemData> ser = null;
        ser = JsonConvert.DeserializeObject<List<SerializeItemData>>(aJsonScene);

        foreach (var e in ser)
        { 
            //try //temp fix for items that aren't connected to prefabs
            {
                //Debug.Log("creating item " + e.prefabName);
                ItemInternal create = new ItemInternal();
                create.prefabName = e.prefabName;
                create.generationSeed = e.generationSeed;
                create.BuildItem();
                create.Go.transform.position = e.position;
                create.Go.transform.localScale = e.scale;
                create.Go.transform.rotation = e.rotation;
            }
            //catch
            {
                //Debug.Log("failed creating " + e.prefabName);
            }
            //TODO store the ItemInternal somewhere
        }

    }

    public static void TestSerialize()
    {
        string scene = SerializeScene();
        //Debug.Log(scene);
        var items = GameObject.FindObjectsOfType<ItemBehaviour>().Where(e => e.gameObject.activeInHierarchy).Select(e => e.ItemInternal);
        foreach (var e in items)
            GameObject.Destroy(e.Go);
        DeserializeScene(scene);
    }

    //TODO json serializable
    class SerializeItemData
    {
        public string prefabName;
        public int generationSeed;
        public Vector3 position;
        public Vector3 scale; //watch out for parent/child local scale type problems...
        public Quaternion rotation;
    }
}
