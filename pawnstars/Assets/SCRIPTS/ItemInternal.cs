﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

//ITEM CREATION PIPELINE
//ItemPrefabBase or derived classes are stored on prefabs. Instantiate from ItemInternal.cs. ItemPrefabBase/derived sets up parameters in ItemInternal and then removes itself.
//use something to indacet game object is an item??? (tags or script???)


public class ItemInternal  
{
    public string prefabName;

    //item generation parameters
    public int generationSeed = -1;
    
    
    //make sure these ARE NOT serialized
    public string Name { get; set; }
    public float BaseValue { get; set; }
    public string Description { get; set; }
    public Action SpecialBehaviour { get; set; }
    public GameObject Go { get; set; }

    public void BuildItem(bool forceFall = false)
    {
        var temp = Resources.Load("ITEMS/" + prefabName, typeof(GameObject)) as GameObject;
        if (temp == null)
            throw new Exception("Could not load item " + prefabName);
        var prefabBehaviour = temp.GetComponent<ItemPrefabBase>();
        if (forceFall)
            prefabBehaviour.fixedPosition = false;
        Go = BuildItem_internal(prefabBehaviour);
    }

    public void DestroyItem()
    {
        if (Go != null)
        {
            GameObject.Destroy(Go);
            SpecialBehaviour = null;
        }
    }

    public static IEnumerable<Transform> GetChildrenRecursive(Transform aObject)
    {
        List<Transform> r = new List<Transform>();
        for (int i = 0; i < aObject.childCount; i++)
        {
            r.AddRange(GetChildrenRecursive(aObject.transform.GetChild(i)));
        }
        r.Add(aObject);
        return r;
    }

    public static ItemInternal[] BuildAllItemsInScene(Transform aParent)
    {
        List<ItemInternal> r = new List<ItemInternal>();
        foreach (var e in GetChildrenRecursive(aParent.transform))
        {
            var ipb = e.GetComponent<ItemPrefabBase>();
            if (ipb != null)
            {
                ItemInternal add = new ItemInternal();
                var go = add.BuildItem_internal(ipb);
                r.Add(add);
            }
        }
        return r.ToArray();
    }

    GameObject BuildItem_internal(ItemPrefabBase aBase)
    {
        prefabName = aBase.gameObject.name.Split(' ').First(); //NOTE this relies on the gameobject name being the same as the prefab name
        var fixedPosition = aBase.fixedPosition;
        var r = aBase.Build(this);
        var ib = r.AddComponent<ItemBehaviour>();
        ib.ItemInternal = this;
        ib.Initialize();
        if (fixedPosition)
            ib.BeginStationary();
        else
            ib.BeginFalling();
        return r;
    }
}
