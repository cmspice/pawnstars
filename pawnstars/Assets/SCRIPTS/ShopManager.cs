using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class ShopManager : MonoBehaviour 
{
	public delegate void ShopManagerEvent();
	public static event ShopManagerEvent OnShopLoaded;
	public static event ShopManagerEvent OnShopUnloaded;

	Dictionary<string, Shop> shops = new Dictionary<string, Shop> ();
    public string CurrentShop { get; private set; }

	Transform m_ShopParent;
	Transform m_environment;
	Transform m_spawnPoint;
	Transform m_PlayerCharacter;
	GameObject m_PlayerCharacterPrefab;

    public FPC FPC { get { return m_PlayerCharacter.GetComponent<FPC>(); } }

	bool loadedLevelFlag = false;

	public bool debugMode;
	public string[] debugScenes;

    public bool IsPlayerShop { get { return CurrentShop == "SHOP - PLAYER"; } }


	void Awake()
	{

        //TODO move this outside of shop manager. Shop manager should handle ONLY loading/unloading of shops
        m_PlayerCharacter = new GameObject("FPC").transform;
        FPC playerCharacter = m_PlayerCharacter.gameObject.AddComponent<FPC>();
        playerCharacter.Initialize();
	}

    //TODO move this outside of shop manager. Shop manager should handle ONLY loading/unloading of shops
    void SetPlayerStartPosition()
    {
        FPC playerCharacter = m_PlayerCharacter.gameObject.GetComponent<FPC>();
        playerCharacter.StartRotation = m_spawnPoint.rotation;

        //later if we want to have multiple spawn points in a store, we should implement some way of choosing them.
        m_PlayerCharacter.position = m_spawnPoint.position;
        m_PlayerCharacter.GetComponent<Rigidbody>().velocity = Vector3.zero;
        m_PlayerCharacter.gameObject.SetActive(true);
    }

    //this will delete all items that were part of the scene
    public void UnloadCurrentScene()
    {
        //Debug.Log(CurrentShop);
        if(CurrentShop != null && CurrentShop != "")
            UnloadScene(CurrentShop);
        CurrentShop = "";
    }

	//should be called by MAIN to initialize shop mode
	public void LoadShop(string ShopToLoad)
	{
        if (ShopToLoad == CurrentShop)
            return;

		if (!shops.ContainsKey (ShopToLoad))
		{
			//if this is the first time we've opened the shop, load the scene asynchronously
			StartCoroutine (LoadScene (ShopToLoad));
		} 
		else
		{
			OpenExistingShop(shops[ShopToLoad]);
		}
	}

	void OpenExistingShop(Shop shop)
	{
		m_ShopParent = shop.parent;
		m_environment = shop.environment;
		m_spawnPoint = shop.spawnPoint;

		m_ShopParent.gameObject.SetActive (true);
		SetPlayerStartPosition ();
	}

	void OnGUI()
	{
		if (debugMode) 
		{
			for(int i = 0; i!= debugScenes.Length; i++)
			{
				if(GUI.Button(new Rect(10, 10 + 40 * i, 100,30), debugScenes[i]))
				{
					LoadShop(debugScenes[i]);
				}
			}
		}
	}

    GameObject[] AllGameObjectsInHierarchy()
    {
        return UnityEngine.Object.FindObjectsOfType<GameObject>().Where(e => e.activeInHierarchy).ToArray();
    }

	IEnumerator LoadScene(string SceneToLoad)
	{
		Shop shop = new Shop ();
        
		AsyncOperation async = Application.LoadLevelAdditiveAsync(SceneToLoad);
		yield return async;

        MAIN.sInst.CutsceneManager.ClearCurrent();

        yield return null;

		shop.parent = GameObject.Find ("NEW SHOP").transform;
		shop.parent.name = SceneToLoad;
		m_ShopParent = shop.parent;

		shop.environment = m_ShopParent.FindChild ("ENVIRONMENT");
		m_environment = shop.environment;
		shop.spawnPoint = m_ShopParent.FindChild("SPAWNPOINT");
		m_spawnPoint = shop.spawnPoint;

		shops.Add (SceneToLoad, shop);
        MAIN.ShowCursor = false;
		yield return new WaitForFixedUpdate ();

		SetPlayerStartPosition ();

        ItemInternal.BuildAllItemsInScene(shop.parent);

        CurrentShop = SceneToLoad;

		MAIN.sInst.PokedexManager.ShowPokedex = true;
        Debug.Log("Loaded scene " + SceneToLoad);


		if (OnShopLoaded != null)
			OnShopLoaded ();
	}

    void UnloadScene(string aScene)
    {
        //TODO do we need to do anything else???
        //like unregister all items for example??
        GameObject.DestroyImmediate(shops[aScene].parent.gameObject);
        shops.Remove(aScene);
    }



	[System.Serializable]
	public class Shop
	{
		public string name;
		public Transform parent;
		public Transform environment;
		public Transform spawnPoint;
		public Transform carPosition;
	}
}
