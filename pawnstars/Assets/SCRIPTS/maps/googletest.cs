/****************
 * {**************************
  * uWebKit 
  * (c) 2014 THUNDERBEAST GAMES, LLC
  * http://www.uwebkit.com
  * sales@uwebkit.com
*******************************************/

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Simple menu for WebGUI Example
/// </summary>
public class googletest : MonoBehaviour
{
    UWKWebView view;

    void Start ()
    {       

        JSObject.SetProperty ("MyJSObject", "unityVersion", Application.unityVersion);

        view = gameObject.GetComponent<UWKWebView>();
        view.JSMessageReceived += onJSMessage;

        //Debug.Log(UWKWebView.GetApplicationDataURL() + "/StreamingAssets/test.html");
        //view.LoadURL(UWKWebView.GetApplicationDataURL() + "/StreamingAssets/test.html");
        view.LoadURL(UWKWebView.GetApplicationDataURL() + "/StreamingAssets/googletest.html");
        //view.LoadURL(UWKWebView.GetApplicationDataURL() + "/StreamingAssets/uWebKit/Examples/JavascriptExample.html");
        //view.LoadURL("https://www.google.com/maps/place/Tennessee/@35.8141386,-88.2216235,7z/data=!3m1!4b1!4m2!3m1!1s0x88614b239e97cf03:0x33e20c1a5819156");

        view.LoadFinished += e => Debug.Log("finished loading");
        view.JSConsole = delegate(UWKWebView vw, string message, int line, string source) { Debug.Log("ln:"+line + " " +message); };
        
        

        //view.SendJSMessage("ExampleMessage", "Javascipt Message " + messageCount++);
    }


    void onJSMessage(UWKWebView view, string message, string json, Dictionary<string, object> values)
    {
        if (message == "UNITY_JS_DEBUG")
            Debug.Log(json);

        //if (message == "MarkerClicked")
            //Debug.Log(json);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
            view.LoadURL(UWKWebView.GetApplicationDataURL() + "/StreamingAssets/googletest.html");
    }
}