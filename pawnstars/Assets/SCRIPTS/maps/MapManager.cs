﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;


public class MapCoordinate
{
    public float lat { get; set; }
    public float lng { get; set; }
 
    //CAN DELETE
    /*public string ToString()
    {
        return "{lat: " + lat.ToString("0.000000") + ", lng: " + lng.ToString("0.000000") + "}";
    }*/
}
public class MapLocation
{
    public string title{get;set;}
    public string description{get;set;}
    public string city{get;set;}
    public string address{get;set;}
    public MapCoordinate gps { get; set; }

    [JsonIgnore]
    public string sceneName { get; set; }
    [JsonIgnore]
    public string cutsceneName { get; set; }
    [JsonIgnore]
    public bool visited { get; set; }


    //CAN DELETE
    /*public string ToString() //as JSON object
    {
        return
            "{" +
            "title: \"" + title + "\", " +
            "gps: " + gps.ToString() + "," +
            "description: \"" + description + "\", " +
            "address: \"" + address + "\", " +
            "city: \"" + city + "\"}";
    }*/

    public MapLocation()
    {
        title = "";
        description = "";
        city = "";
        address = "";
        sceneName = "";
        cutsceneName = "";
    }
}

public static class MapLocationList
{
    public static MapLocation sHome = 
        new MapLocation() {
            title = "Flagstaff",
            gps = new MapCoordinate(){lat = 35.1982836f, lng = -111.639244f},
			description = "Flagstaff's early economy was based on the lumber, railroad, and ranching industries. Today, the city remains an important distribution hub for companies such as Nestlé Purina PetCare, and is home to Lowell Observatory, The U.S. Naval Observatory, the United States Geological Survey Flagstaff Station, and Northern Arizona University. Flagstaff has a strong tourism sector, due to its proximity to Grand Canyon National Park, Oak Creek Canyon, the Arizona Snowbowl, Meteor Crater, and historic Route 66. The city is also a center for medical device manufacturing, since Flagstaff is home to W. L. Gore and Associates.",
            address = "Flagstaff",
            city = "Panguitch, UT 84759",
            sceneName = "SHOP - PLAYER",
            cutsceneName = "START"
        };
    public static Dictionary<string, MapLocation> sLocations = new Dictionary<string, MapLocation>()
    {
        {
            "Flagstaff", 
            new MapLocation() {
                title = "Flagstaff",
                gps = new MapCoordinate(){lat = 35.1982836f, lng = -111.639244f},
				description = "Flagstaff's early economy was based on the lumber, railroad, and ranching industries. Today, the city remains an important distribution hub for companies such as Nestlé Purina PetCare, and is home to Lowell Observatory, The U.S. Naval Observatory, the United States Geological Survey Flagstaff Station, and Northern Arizona University. Flagstaff has a strong tourism sector, due to its proximity to Grand Canyon National Park, Oak Creek Canyon, the Arizona Snowbowl, Meteor Crater, and historic Route 66. The city is also a center for medical device manufacturing, since Flagstaff is home to W. L. Gore and Associates. A humble place for your humble home.",
                address = "Flagstaff",
                city = "Panguitch, UT 84759",
                sceneName = "SHOP - PLAYER",
                cutsceneName = "Flagstaff"
            }
        },
        {
            "Bryce Canyon Trading Post", 
            new MapLocation() {
                title = "Bryce Canyon Trading Post",
                gps = new MapCoordinate(){lat = 37.7488208f, lng = -112.364738f},
				description = "A tourist trading post just before Bryce Canyon National Park. There still good things to be bought here",
                address = "2938 UT-12",
                city = "Flagstaff, AZ, 86011",
                cutsceneName = "brycecanyontradingpost"
            }
        },
        /*{
            "Cowboy Collectibles", 
            new MapLocation() {
                title = "Cowboy Collectibles",
                gps = new MapCoordinate(){lat = 37.8231337f, lng =-112.435654f},
                description = "http://cowboycollectiblesutah.com/",
                address = "57 N Main St",
                city = "Panguitch, UT 84759",
                sceneName = "SHOP - Desert Yard Sale",
                cutsceneName = "desertyardsale"
            }
        },*/
        {
            "Gila County Desert Swap Meet", 
            new MapLocation() {
                title = "Gila County Desert Swap Meet",
                gps = new MapCoordinate(){lat = 33.139977f, lng = -110.791100f},
                description = "A swap meet out in the middle of the dessert.",
                address = "Hwy 77",
                city = "Gila County, AZ",
                sceneName = "SHOP - Desert Yard Sale",
                cutsceneName = "Gila County Desert Swap Meet"
            }
        },
        {
            "Dinosaur Tracks", 
            new MapLocation() {
                title = "Dinosaur Tracks",
                gps = new MapCoordinate(){lat = 36.1024258f, lng = -111.3277141f},
                description = "A rather barren turn off on the 160 with a few shacks operated by a family of Navajo Indians. Visitors are invited on a donation-based guided tour to see dinosaur tracks and fossils.",
                address = "Hwy 160",
                city = "Arizona 86045",
                cutsceneName = "dinosaurtracks"
            }
        },
        {
            "Navajo Jewelry Stands", 
            new MapLocation() {
                title = "Navajo Jewelry Stands",
                gps = new MapCoordinate(){lat = 36.075961f, lng = -111.391879f},
                description = "At the T interseciton of the 89 and 160 is a group of a dozen stands for Navajo Indians to sell their hand made Jewelry. These are easy targets for hate crimes.",
                address = "Hwy 89 and Hwy 160",
                city = "Cameron, AZ 86020",
                sceneName = "SHOP - Jewelery",
				cutsceneName = "Navajo Jewelry Stands"
            }
        },
        {
            "Arcosanti", 
            new MapLocation() {
                title = "Arcosanti",
                gps = new MapCoordinate(){lat = 34.3425315f, lng = -112.100554f},
                description = "Founded by Paolo Soleri in 1965, Arcosanti is a not-for-profit educational organization devoted to the support of Soleri's noted architectural and urban planning research.",
                address = "Arcosanti",
                city = "Arcosanti, AZ 86333",
                cutsceneName = "arcosanti"
            }
        },
        {
            "Savers Thrift Store", 
            new MapLocation() {
                title = "Savers Thrift Store",
                gps = new MapCoordinate(){lat = 33.494706f, lng = -112.29192f},
                description = "A huge thrift store West of downtown Phoenix. From the outside, the building looks like a department store.",
                address = "10720 W Indian School Rd",
                city = "Phoenix, AZ 85037",
                sceneName = "SHOP - GOODWILL"
            }
        },
		{
			"Grand Canyon - South Rim Park Headquarters", 
			new MapLocation() {
				title = "Grand Canyon - South Rim Park Headquarters", 
				gps = new MapCoordinate(){lat = 36.054952f, lng = -112.122436f},
				description = "Beyond its scenic overlooks, this mile-deep geologic wonder features hikes, mule rides & rafting.",
				address = "Grand Canyon Village",
				city = "Grand Canyon Village, AZ 86023",
				cutsceneName = "Grand Canyon South"
			}
		},

		{
			"Ely Ghost Train Museum", 
			new MapLocation() {
				title = "Ely Ghost Train Museum",
				gps = new MapCoordinate(){lat =   39.257867f, lng = -114.869532f},
                description = "Ely was founded as a stagecoach station along the Pony Express and Central Overland Route, and is home to the Nevada Northern Railway Museum",
                address = "10720 W Indian School Rd",
                city = "Phoenix, AZ 85037",
                cutsceneName = "elytrain"
            }
        }  ,
        {
            "Seemore's Polar Parlor", 
            new MapLocation() {
                title = "Seemore's Polar Parlor",
                gps = new MapCoordinate(){lat = 36.205135f, lng = -116.014362f},
                description = "Self proclaimed \"World's Tallest Ice Cream Stand\"",
                address = "70 E Hwy 372",
                city = "Pahrump, NV 98048",
                cutsceneName = "polarParlor"
            }
        },
        {
            "Grand Canyon - North Rim Lookout", 
            new MapLocation() {
                title = "Grand Canyon - North Rim Lookout",
                gps = new MapCoordinate(){lat =   36.210399f, lng =  -112.06074f},
                description = "One of many Scenic Overlooks overlooking the canyon.",
                address = "10720 W Indian School Rd",
                city = "Phoenix, AZ 85037",
                cutsceneName = "grandCanyon"
            }
        },
        {
            "Malakoff Diggins", 
            new MapLocation() {
                title = "Malakoff Diggins",
                gps = new MapCoordinate(){lat =   39.365226f, lng = -120.924195f},
                description = "Historic Hydrauling Mining Site",
                address = "10720 W Indian School Rd",
                city = "Phoenix, AZ 85037",
                cutsceneName = "malakoffDiggins"
            }
        }
    };
}
public class MapManager : MonoBehaviour {
    GameObject main;
    WebTexture webTexture;
    UWKWebView webView;
    Camera webCamera;
    Shader mapShader;
    bool webViewLoaded = false;

	float returnCost;
    float travelCost;

    public event System.Action<MapLocation> TravelEvent = (e) => {};

    public bool ShowMap
    {
        get
        {
            return webViewLoaded && webCamera.enabled;
        }
        set
        {
            if (value && webView == null)
            {
                StartCoroutine(CreateWebView());
            }
            else if (webViewLoaded)
            {
                if (value)
                {
                    webTexture.MouseEnabled = true;
                    webCamera.enabled = true;
                }
                else
                {
                    webTexture.MouseEnabled = false;
                    webCamera.enabled = false;
                }
            }
        }
    }

    

    public void Initialize()
    {
        webCamera = new GameObject("webCamera").AddComponent<Camera>();
        webCamera.transform.position = new Vector3(0, 1000, 0);
        webCamera.depth = 100;
        webCamera.clearFlags = CameraClearFlags.Depth;

        mapShader = MAIN.sInst.reduceColorShader;
    }
    


 

	void Update () {
        //TODO this is just temp behaviour
        /*
        if (Input.GetKeyDown(KeyCode.M))
        {    
            if (webViewLoaded && webCamera.enabled == false) //if we are loaded and hidden, show the map
                ShowMap = true;
            else //otherwise load/reload the map
                StartCoroutine(CreateWebView()); 
        }*/
	}

    IEnumerator CreateWebView()
    {
        webViewLoaded = false;
        webCamera.enabled = false;

        if (main != null)
        {
            GameObject.DestroyImmediate(main);
            GameObject.DestroyImmediate(GameObject.Find("UWKCore"));
        }
        yield return null;

        main = GameObject.CreatePrimitive(PrimitiveType.Quad);
        main.GetComponent<Renderer>().material.shader = mapShader;
        webTexture = main.AddComponent<WebTexture>();
        webTexture.raycastCamera = webCamera;
        webTexture.KeyboardEnabled = true;
        webTexture.MouseEnabled = false;
        webTexture.Rotate = false;
        webTexture.HasFocus = true;
        webTexture.AlphaMask = false;

        webView = main.AddComponent<UWKWebView>();
        webView.URL = "";
        webView.CurrentWidth = 1024;
        webView.CurrentHeight = 1024;
        webView.MaxWidth = 1024;
        webView.MaxHeight = 1024;
        webView.ScrollSensitivity = 1;
        webView.LoadURL(UWKWebView.GetApplicationDataURL() + "/StreamingAssets/googletest.html");

        
        webTexture.transform.position = webCamera.transform.position + webCamera.transform.forward * 1;
        //webTexture.transform.Rotate(Vector3.forward, 180);
        //webTexture.transform.Rotate(Vector3.left, 90);

        webView.JSConsole = delegate(UWKWebView vw, string message, int line, string source) { Debug.Log("ln:" + line + " " + message); };
        webView.JSMessageReceived += onJSMessage;

        webView.LoadFinished += e => { 
			SetCASH();
            SetMaps(); 
            webViewLoaded = true; 
            ShowMap = true;
            MAIN.ShowCursor = true;
        };
    }

    void onJSMessage(UWKWebView view, string message, string json, Dictionary<string, object> values)
    {
        if (message == "UNITY_JS_DEBUG")
            Debug.Log(json);

		if (message == "TravelCost") {
            travelCost = System.Convert.ToSingle(json);
		}
        if (message == "TravelToLocation")
        {
			//Debug.Log(returnCost + " " + MAIN.sInst.CASH);

			if(travelCost > MAIN.sInst.CASH)
			{
				webView.SendJSMessage("MONEY", null);
				Debug.Log("NOT ENOUGH MONEY");
                MAIN.sInst.CutsceneManager.Error("!!ERROR!!\nYou can't afford that.");
				return;
			}

            MAIN.sInst.CASH -= travelCost;
            json = json.Substring(1, json.Length - 2); //uwebkit adds "" so we remove them
            //Debug.Log(json);
            var location = MapLocationList.sLocations[json];
            //Debug.Log(Newtonsoft.Json.JsonConvert.SerializeObject(location));
            TravelEvent(location);
        }

		
		if (message == "ReturnCost") {
            returnCost = System.Convert.ToSingle(json);
            //Debug.Log("RETURN COST " + returnCost);
		}
		
		if (message == "FORCETRAVEL") {
			json = json.Substring(1, json.Length - 2); //uwebkit adds "" so we remove them
			//Debug.Log(json);
			var location = MapLocationList.sLocations[json];
			//Debug.Log(Newtonsoft.Json.JsonConvert.SerializeObject(location));
			TravelEvent(location);
			Debug.Log ("FORCE TRAVEL");
		}
		
	}
	
	public void SetCASH()
	{
        if (webView != null)
		    webView.SendJSMessage("SetMoney", Newtonsoft.Json.JsonConvert.SerializeObject(MAIN.sInst.CASH.ToString()));
	}

    void SetMaps()
    {
        foreach (var e in MapLocationList.sLocations)
        {
            webView.SendJSMessage("AddLocation", Newtonsoft.Json.JsonConvert.SerializeObject(e.Value));
        }
        SetCurrentLocation("Flagstaff");
		if (webView != null)
			webView.SendJSMessage("SetHomeLocation", Newtonsoft.Json.JsonConvert.SerializeObject(MapLocationList.sLocations["Flagstaff"]));
    }

    public void SetCurrentLocation(string aLocation)
    {
        MapLocationList.sLocations[aLocation].visited = true;
        if (webView != null)
            webView.SendJSMessage("SetCurrentLocation", Newtonsoft.Json.JsonConvert.SerializeObject(MapLocationList.sLocations[aLocation]));
    }
}
