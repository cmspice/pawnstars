﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class FPC : MonoBehaviour
{
	CapsuleCollider m_collider;
	Rigidbody m_rigidbody;
	Transform m_cameraTransform;
	Camera m_camera;

    GameObject mCanvas;
    Image mOn;
    Image mOff;

	public float walkSpeed = 4f;
	public float lookSensitivity = 1;
	public Quaternion StartRotation
	{
		get
		{
			return _StartRotation;
		}

		set	
		{
			_StartRotation = StartRotation;
			m_cameraTransform.rotation = StartRotation;
		}
	}

    public bool Crosshair
    {
        set
        {
            if (mOff != null && mOn != null)
            {
                mOn.gameObject.SetActive(value);
                mOff.gameObject.SetActive(!value);
            }
        }
    }

	Quaternion _StartRotation;

	public ItemBehaviour currentlyHeldItem;
	Vector3 heldItemOffset = new Vector3(0, -0.25f,1f);

	// Use this for initialization
	public void Initialize () 
	{
		m_collider = gameObject.AddComponent<CapsuleCollider> ();
		m_rigidbody = gameObject.AddComponent<Rigidbody> ();
		m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
	//	m_cameraTransform = new GameObject ("FPC Camera").transform;
		m_cameraTransform = (Instantiate (Resources.Load<GameObject> ("SYSTEM_PREFABS/FPC Camera")) as GameObject).transform;
		m_cameraTransform.parent = transform;
		m_cameraTransform.localPosition = Vector3.up * 0.5f;
		m_cameraTransform.rotation = StartRotation;
		m_camera = m_cameraTransform.gameObject.GetComponent<Camera> ();
        m_camera.tag = "MainCamera";

        mCanvas = Instantiate(Resources.Load<GameObject>("SYSTEM_PREFABS/CrosshairUI")) as GameObject;
        mOff = mCanvas.transform.FindChild("off").GetComponent<Image>();
        mOn = mCanvas.transform.FindChild("on").GetComponent<Image>();
	}

	// Update is called once per frame
	void Update () 
	{
        if (currentlyHeldItem == null)
        {
            DetectItems();
        }
        else
        {
            RotateItem();
        }
	}

	void FixedUpdate()
	{

	//	m_cameraTransform.Rotate (new Vector3 (Input.GetAxis ("Mouse Y") * lookSensitivity, Input.GetAxis ("Mouse X") * lookSensitivity, 0));
		m_cameraTransform.RotateAround (m_cameraTransform.position, Vector3.up, Input.GetAxis ("Mouse X") * lookSensitivity);
        var motiony = Input.GetAxis("Mouse Y");
        var dp = Vector3.Dot(Vector3.up, m_cameraTransform.forward);
        var limity = .8f;
        if((motiony <= 0 && dp >= -limity) || (motiony >= 0 && dp <= limity))
		    m_cameraTransform.RotateAround (m_cameraTransform.position, m_cameraTransform.right, -motiony * lookSensitivity);

		Vector3 inputV = new Vector3 (m_cameraTransform.forward.x, 0, m_cameraTransform.forward.z).normalized * Input.GetAxis ("Vertical");
		Vector3 inputH = new Vector3 (m_cameraTransform.right.x, 0, m_cameraTransform.right.z).normalized * Input.GetAxis ("Horizontal");
		Vector3 input = (inputV + inputH) * Time.deltaTime;

		m_rigidbody.MovePosition (transform.position + input);

	}

	void DetectItems()
	{

		RaycastHit hit;
		if (Physics.Raycast (m_cameraTransform.position, m_cameraTransform.forward, out hit, 2f))
		{
			//Debug.Log("HIT" + hit.collider.name);
			if(Input.GetButtonDown("Fire1"))
			{

                if (hit.collider.attachedRigidbody == null)
                    return;
				currentlyHeldItem = hit.collider.attachedRigidbody.GetComponent<ItemBehaviour>();
				if(currentlyHeldItem == null) 
                    return;

				heldItemOffset = m_cameraTransform.InverseTransformPoint(currentlyHeldItem.transform.position);
				if(currentlyHeldItem != null)
					Debug.Log("Got Item! " + currentlyHeldItem.ItemName);
				currentlyHeldItem.HeldPosition = m_cameraTransform.TransformPoint (heldItemOffset);
				currentlyHeldItem.BeginHeld();
			}
		}
	}

	void RotateItem()
	{

		currentlyHeldItem.HeldPosition = m_cameraTransform.TransformPoint (heldItemOffset);
		if (Input.GetButtonUp ("Fire1")) {
			currentlyHeldItem.BeginFalling();
			currentlyHeldItem = null;

		}
	}
}
