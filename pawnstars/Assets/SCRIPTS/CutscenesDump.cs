﻿using UnityEngine;
using System.Collections.Generic;

public class CutsceneInfo
{
    public string Scene { get; set; }
    public string Filename { get;set; }
    public List<string> Text { get; set; }

    public CutsceneInfo()
    {
        Scene = "";
        Filename = "";
        Text = new List<string>();
            
    }
}

public static class CutsceneDump
{
    public static string sNugent =
        "from: b9nvb-5232912585@sale.craigslist.org\nsubject: RE: Ted Nugent Pinball Machine\n>Hey enjoy your silly new pinball machine. Ciao.";

    public static Dictionary<string, CutsceneInfo> sCutscenes = new Dictionary<string, CutsceneInfo>()
    {
        {"default",new CutsceneInfo(){Scene = "INTERSTITIAL SCENEN", Text = new List<string>(){
                "A long drive through the country.",
                "Nothing here of interest."
            }}
        },
        {"START",new CutsceneInfo(){Scene = "HOME", Text = new List<string>(){
                "from: b9nvb-5232912585@sale.craigslist.org\nsubject: RE: Ted Nugent Pinball Machine\n>Hi,\nyes it's still available. I'm out in the AZ desert. Do you want to come pickup? I'm out in Christmas AZ. $415 cash firm.",
                "\n---------------------------\n\nfrom: antiquecollector23@gmail.com\nsubject: RE: Ted Nugent Pinball Machine\n>Yes it's a bit of a trek but I'll come pick it up. I don't trust movers to handle the machine with proper care. Where can I meet you? Free all week.",
                "\n---------------------------\n\nfrom: b9nvb-5232912585@sale.craigslist.org\nsubject:RE: Ted Nugent Pinball Machine\n>OK, lets meet at the Gila County swap meet Tuesday. \nSee you.",
                "\n---------------------------\n\nfrom: antiquecollector23@gmail.com\nsubject: RE: Ted Nugent Pinball Machine\n>Sounds good, see you.",
                "\n-----click to continue-----",
                "<clear>::INSTRUCTIONS::",
                "mouse to look",
                "WASD to move",
                "press B to buy items.",
                "press N to drop items (you do not get refunded).",
                "press M to open up your map and travel.",
                "Travel and meet b9nvb-5232912585@sale.craigslist.org At the Gila County swap meet to pick up the pinball machine and return.",
                "Maybe stop by and see the attractions on the way.",
                "You have $1300 in the bank.",
                "Don't go over budget!",
                "\n-----click to continue-----"

            }}
        },
        {"Flagstaff",new CutsceneInfo(){Scene = "INTERSTITIAL SCENEN", Text = new List<string>(){
                "Welcome home...",
                "If only you had a Ted Nugent Pinball Machine",
                "\n-----click to continue-----"
            }}
        },
        {"delicatearch",new CutsceneInfo(){Filename ="CUTSCENE/delicate_arch.jpg", Text = new List<string>(){
                "Delicate Arch is a 65-foot-tall (20 m) freestanding natural arch located in Arches National Park near Moab, Utah, USA.",
                "It is the most widely recognized landmark in Arches National Park and is depicted on Utah license plates and on a postage stamp commemorating Utah's centennial anniversary of admission to the Union in 1996.",
                "The Olympic torch relay for the 2002 Winter Olympics passed through the arch.",
                "\n-----click to continue-----"
            }}
        },
        {"arcosanti",new CutsceneInfo(){Scene = "ARCOSANTI", Text = new List<string>(){
                "Arcosanti, founded by Paolo Soleri in 1956.",
                "Soleri coined the term \"Arcology\" to describe his designs for ecologically sound human habitats in 1969.",
                "Arcosanti was an experiemental city to implement and test Soleri's designs.",
                "Today, after Soleri's death, the city continues his experiments fostering a community of over 100 people both as permanent residents and students",
                "\n-----click to continue-----"
            }}
        },
        {"dinosaurtracks",new CutsceneInfo(){Scene = "DINOTRACKS", Filename ="CUTSCENE/dinosaur_tracks.jpg", Text = new List<string>(){
                "A billboard on the side of Hwy 160 appears in the distance",
                "The highways are littered with such signs but this one catches your attention for some reason",
                "You turn off following the large arrow on the sign. All you see is a red truck and a few shacks",
                "You stop the car. A large Navajo man approaches your vehicle and asks if you want to see the tracks",
                "You agree. He takes you on the tour showing you a few claw shaped imprints and fossilized bones in the nearby area",
                "He talks a little about his own life and other visitors who come through",
                "You weren't obligated too but you felt compelled to buy some Navajo hand-crafted jewelry as you depart.",
                "It looks nice and wasn't too expensve",
                "\n-----click to continue-----"
            }}
        },
		{"brycecanyontradingpost",new CutsceneInfo(){Scene = "BRYCECANYONTRADING", Text = new List<string>(){
                "A modern trading post right on the fork of the no through road to Bryce Canyon.",
                "Run by a humble white middle-aged woman who seems very content with her life.",
                "Nothing here catches your interest. You help yourself to some free coffee on your way out",
                "\n-----click to continue-----"
            }}
        },
		{"Navajo Jewelry Stands",new CutsceneInfo(){Scene = "INTERSTITIAL SCENEN", Text = new List<string>(){
				"You approach a T in the highway. There are several wooden stalls at the T and a few signs but otherwise there is nothing for miles.",
				"It's really early in the morning and the Navajo natives have yet to arrive to sell their handmade jewelery at these stalls.",
				"A foul odor fills the air.",
				"You park in front to examine the stalls. A pile of plastic bags containing white used diapers are strewn in front of the ground.",
				"On the tables, paper plates with food trash and \"Navajo Tacos\" spelled out in ketchup.",
                "\n-----click to continue-----"
			}}
		},
		
		{"Grand Canyon South",new CutsceneInfo(){Scene ="GRAND CANYON", Text = new List<string>(){
					"The Grand friggin Canyon.",
					"Normally it's a $30 National Park entry fee.",
					"You got in for free by telling them you are part Navajo.",
					"The lady at the booth looked skeptical but she still let you in.",
                    "\n-----click to continue-----"
				}}
		},

		{"elytrain",new CutsceneInfo(){Scene = "ELY GHOST TRAIN", Text = new List<string>(){
				"You pull up to a small museum, housing a series of trains that look quite old.",
				"The front door is plastered with stickers for various awards - \"NEVADA 2012 ATTRACTION OF THE YEAR\"",
				"A length of tracks runs past the back of the museum and up into the hills",
				"Inside you thumb through an informational pamphlet - Evidently the museum houses some of the best preserved steam locomotives in America",
				"You pick up a model engine and check out some of the exhibits.",
                "\n-----click to continue-----"
			}}
		},
		{
            "polarParlor",new CutsceneInfo(){
                Scene = "PAHRUMP POLAR PARLOR", Text = new List<string>(){
				"In the distance you see what looks like a yellow castle with a big dollop of soft-serve on top",
                "You turn off the local radio program you were listening to - \"Coast To Coast with Art Bell\" - and drive closer.",
				"A young woman in a baseball cap takes your order from behind the window.",
                "Probably not Seemore.",
				"Leaning against your truck, you remember how your kids tease you about your sweet tooth.",
				"You quickly finish your cone before it starts to melt.",
				"Delicious.",
                "\n-----click to continue-----"
			}}
		},
		{
            "grandCanyon",new CutsceneInfo(){Scene = "GRAND CANYON", Text = new List<string>(){
				"Exhausted from a long drive, you get out at a scenic overlook to stretch your legs",
                "\"It's pretty big, alright\" you think to yourself, looking over the massive canyon.",
                "\n-----click to continue-----"
			}}
		},
		{
            "malakoffDiggins",new CutsceneInfo(){Scene = "MalakoffDiggins", Text = new List<string>(){
				"Exhausted from a long drive, you get out at a scenic overlook to stretch your legs",
                "\"It's pretty big, alright\" you think to yourself, looking over the massive canyon.",
                "\n-----click to continue-----"
			}}
		},
        {
            "Gila County Desert Swap Meet",new CutsceneInfo(){Scene = "INTERSTITIAL SCENEN", Text = new List<string>(){
				"A fun little swap out in the desert near Gila County.",
                "Whoever decided on this place to meet is a total idiot. How did so many people agree to meet out here in the middle of nowhere?",
                "Yet here you are. I guess the novelty of such an event speaks for its itself.",
                "This is where you'll meet b9nvb-5232912585@sale.craigslist.org to pick up your Ted Nugent Pinball Machine.",
                "Maybe pick up a few souvenirs since you drove all the way out for this.",
                "\n-----click to continue-----"
			}}
		}

        
    };
}
