﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO; 
using System;
using UnityEngine.UI;

public class CutsceneManager : MonoBehaviour {
    Font mDefaultFont;
    Shader mBackdropShader;

    FlatCameraManager mCamera;
    FlatElementImage mCurrent = null;

    List<FlatElementImage> mChoiceBoxes = new List<FlatElementImage>();
    List<FlatElementText> mChoices = new List<FlatElementText>();

    //canvas stuff
    GameObject canvas;
    Text cutsceneText;

    public event Action CutsceneFinished = () => { };

    public bool CanvasVisibility { get { return canvas.activeInHierarchy; } set { canvas.SetActive(value); } }

    GameObject CurrentDiorama { get; set; }

    public string NextScene { get; set; }

    IEnumerator LoadDiorama(string SceneToLoad)
    {
        AsyncOperation async = Application.LoadLevelAdditiveAsync(SceneToLoad);
        yield return async;

        CurrentDiorama = GameObject.Find("NEW SHOP");
        CurrentDiorama.GetComponentInChildren<Camera>().depth = 100;

        Debug.Log("cached " + CurrentDiorama);
        
        MAIN.ShowCursor = false;
		MAIN.sInst.PokedexManager.ShowPokedex = false;
        yield return new WaitForFixedUpdate();

    }
    void UnloadCurrentDiorama()
    {
        NextScene = null;
        if (CurrentDiorama != null)
        {
            GameObject.Destroy(CurrentDiorama);
            CurrentDiorama = null;
        }
    }

    public void Update()
    {
        foreach (var e in mChoiceBoxes)
            e.update(Time.deltaTime);
    }

    public void Initialize()
    {
        mDefaultFont = Resources.Load("CUTSCENE/font2", typeof(Font)) as Font;
        mBackdropShader = MAIN.sInst.cutsceneShader;

        mCamera = new FlatCameraManager(new Vector3(10000, 0, 0), 2);
        mCamera.Width = 1920;
        mCamera.Camera.clearFlags = CameraClearFlags.SolidColor;
        mCamera.Camera.backgroundColor = Color.black;
        mCamera.Camera.enabled = false;
        mCamera.Camera.depth = 9;

        canvas = GameObject.Instantiate(Resources.Load("SYSTEM_PREFABS/TextUI")) as GameObject;
        cutsceneText = canvas.transform.FindChild("Text").gameObject.GetComponent<Text>();
        CanvasVisibility = false;
    }

    public void SetBackdrop(Texture2D aTex)
    {
        mCurrent = new FlatElementImage(aTex, 0);
        mCurrent.HardPosition = mCurrent.SoftPosition = mCamera.Center;
        mCurrent.HardScale = mCurrent.SoftScale = Vector3.one * mCamera.Width / mCurrent.BoundingBox.width;
        mCurrent.HardShader = mBackdropShader;
        mCurrent.update(0);
    }


    public void LoadCutscene(string aName)
    {
        ClearCurrent();
        mCamera.Camera.enabled = true;

        var info = CutsceneDump.sCutscenes[aName];

        //scene cutscene
        if (info.Scene != null && info.Scene != "")
        {
            Debug.Log("Loading diorama " + info.Scene);
            StartCoroutine(LoadDiorama(info.Scene));
        }
        else
        {
            //image cutscene
            //Debug.Log(Path.GetDirectoryName(info.Filename) + "/" + Path.GetFileNameWithoutExtension(info.Filename));
            var cutscene = Resources.Load(Path.GetDirectoryName(info.Filename) + "/" + Path.GetFileNameWithoutExtension(info.Filename), typeof(Texture2D)) as Texture2D;
            //TODO image should fit to screen
            SetBackdrop(cutscene);
        }

        StartCoroutine(CutsceneCoroutine(info.Text));
    }

    public void ClearCurrent()
    {
        UnloadCurrentDiorama();
        mCamera.Camera.enabled = false;

        if (mCurrent != null)
            mCurrent.destroy();
        mCurrent = null;
    }


    public void Error(string aError, bool center = true)
    {
        StartCoroutine(ErrorCoroutine(aError,center));
    }
    IEnumerator ErrorCoroutine(string error, bool center)
    {
        CanvasVisibility = true;
        cutsceneText.text = error + "\n\n-----click to continue-----";
        if(center)
            cutsceneText.alignment = TextAnchor.MiddleCenter;
        else cutsceneText.alignment = TextAnchor.UpperLeft;
        while (true)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                CanvasVisibility = false;
                yield break;
            }
            yield return null;
        }
        
    }

    public IEnumerator CutsceneCoroutine(List<string> text)
    {
        CanvasVisibility = true;
        cutsceneText.alignment = TextAnchor.UpperLeft;
        cutsceneText.text = text[0];
        if (text[0].Length > 40)
            cutsceneText.text = FlatElementText.convert_to_multiline(2, text[0]);
        int i = 0;
        while(i < text.Count-1)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                var write = text[++i];
                if (write.StartsWith("<clear>"))
                {
                    cutsceneText.text = write.Substring(7);
                }
                else
                    cutsceneText.text += "\n" + write;
            }
            yield return null;
        }
        while (true)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                CutsceneFinished();
                if (NextScene != null)
                {
                    Debug.Log("trying to load " + NextScene);
                    MAIN.sInst.ShopManager.LoadShop(NextScene);
                }
                CanvasVisibility = false;
                break;
            }
            yield return null;
        }
        
    }

}
