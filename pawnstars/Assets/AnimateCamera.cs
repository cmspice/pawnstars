﻿using UnityEngine;
using System.Collections;

public class AnimateCamera : MonoBehaviour
{
	public Transform[] points;
	public float cameraSpeed;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine (TraversePoints ());
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void SetTransformTo(Transform target)
	{
        if (target == null) return;
		transform.position = target.position;
		transform.rotation = target.rotation;
	}

	IEnumerator TraversePoints()
	{
		SetTransformTo (points [0]);
		for (int i = 1; i!= points.Length; i++) 
		{
            if (points[i] == null) continue;
			yield return StartCoroutine(AdvanceToPoint(i));
		}
	}
	IEnumerator AdvanceToPoint(int nextPoint)
	{
		while (transform.position!= points[nextPoint].position) 
		{
			transform.position = Vector3.MoveTowards(transform.position, points[nextPoint].position, Time.deltaTime * cameraSpeed);
			yield return new WaitForEndOfFrame();
		}
	}
}
