Shader "Custom/LayeredHSVShiftUnlit" 
{
Properties {
	_Color ("Color", Color) = (1,1,1,1)
	_MainTex ("Color to Shift (RGB) Trans (A)", 2D) = "white" {}
	
	_HueShift ("Hue (0-360)", Float) = 0
	_SatShift ("Saturation", Float) = 1
	_ValShift ("Value (brightness)", Float) = 1
	
	_Tex2 ("Unshifted Color (RGB) Trans (A)", 2D) = "clear" {}
	
	_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	
}

SubShader {
	Lighting Off
	Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
	LOD 200
	
CGPROGRAM

#pragma surface surf Lambert alphatest:_Cutoff
#pragma target 3.0

sampler2D _MainTex;
sampler2D _Tex2;

float _HueShift;
float _SatShift;
float _ValShift;

fixed4 _Color;

   half4 LightingUnlit (SurfaceOutput s, half3 lightDir, half atten) {
           half4 c;
           c.rgb = s.Albedo;
           c.a = s.Alpha;
           return c;
         }

float3 shift_col(float3 RGB, float3 shift)
            {
            float3 RESULT = float3(RGB);
            float VSU = shift.z*shift.y*cos(shift.x*3.14159265/180);
                float VSW = shift.z*shift.y*sin(shift.x*3.14159265/180);
               
                RESULT.x = (.299*shift.z+.701*VSU+.168*VSW)*RGB.x
                        + (.587*shift.z-.587*VSU+.330*VSW)*RGB.y
                        + (.114*shift.z-.114*VSU-.497*VSW)*RGB.z;
               
                RESULT.y = (.299*shift.z-.299*VSU-.328*VSW)*RGB.x
                        + (.587*shift.z+.413*VSU+.035*VSW)*RGB.y
                        + (.114*shift.z-.114*VSU+.292*VSW)*RGB.z;
               
                RESULT.z = (.299*shift.z-.3*VSU+1.25*VSW)*RGB.x
                        + (.587*shift.z-.588*VSU-1.05*VSW)*RGB.y
                        + (.114*shift.z+.886*VSU-.203*VSW)*RGB.z;
               
            return (RESULT);
            }


struct Input {
	float2 uv_Tex2;
	float2 uv_MainTex;
};

void surf (Input IN, inout SurfaceOutput o) 
{
	fixed4 b = tex2D(_Tex2, IN.uv_Tex2) * _Color;
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	
	float3 hsv;
	hsv.x  = _HueShift;
	hsv.y  = _SatShift;
	hsv.z  = _ValShift;
	
	c.rgb = shift_col(c.rgb, hsv);

	o.Emission = b.rgb * b.a + c.rbg * c.a * (1-b.a);
	o.Alpha = c.a;

}
ENDCG
}

Fallback "Transparent/Cutout/Unlit"
}
