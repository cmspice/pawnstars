
var mpg = 3/27;
//var mpg = 10;
var map;
var startingLatLng = {lat: 36.075961, lng: -111.391879};
var homeLocation;
var currentLocation = null;
var directionsService;
var directionsDisplay;
var locationInfoMap = {};
var selectedLocation = null;
var initialized = false;
var travelCost = 0;

Number.prototype.formatMoney = function(c, d, t){
  var n = this, 
  c = isNaN(c = Math.abs(c)) ? 2 : c, 
  d = d == undefined ? "." : d, 
  t = t == undefined ? "," : t, 
  s = n < 0 ? "-" : "", 
  i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
  j = (j = i.length) > 3 ? j % 3 : 0;
  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function UnityJSONStupidConvert(json) //the unity->javascript communication methond is broken so this corrects it... 
{
  var jsonstr = JSON.stringify(json.value).split('\\"').join('"');
  jsonstr = jsonstr.substring(1,jsonstr.length-1);
  return JSON.parse(jsonstr);
}

UWK.subscribe("SetCurrentLocation", function(msgName, json) {
  //UWK.sendMessage("UNITY_JS_DEBUG",UnityJSONStupidConvert(json).gps.lat);
  var data = UnityJSONStupidConvert(json);
  if(currentLocation != null)
  {
    locationInfoMap[(new google.maps.LatLng(currentLocation)).toString()].mk.setIcon("ormarker.png"); //visited
  }
  currentLocation = data.gps;
  locationInfoMap[(new google.maps.LatLng(data.gps)).toString()].mk.setIcon("grmarker.png"); //current
  directionsDisplay.setMap(null);
  
});

UWK.subscribe("SetHomeLocation", function(msgName, json) {
  var data = UnityJSONStupidConvert(json);
  homeLocation = data.gps;
});

UWK.subscribe("AddLocation", function(msgName, json) {
  //UWK.sendMessage("UNITY_JS_DEBUG",UnityJSONStupidConvert(json));
  CreateMarker(UnityJSONStupidConvert(json));
});
UWK.subscribe("SetMoney", function(msgName, json) {
  var money = parseFloat(json.value.substr(1,json.value.length-1));
  $("#finance").html("<p>CASH: $" + parseFloat(money).formatMoney(2, '.', ',') + "  </p>");
  console.log("set money too " + $("#finance").html());
});
UWK.subscribe("MONEY", function(msgName, json) {
  //TODO
  //if(confirm("WARNING, you won't have enough money. Travel anyways?")){
    //UWK.sendMessage("FORCETRAVEL",selectedLocation.place);
  //}
});

var markerList = 
[
  {
    title: "Bryce Canyon Trading Post",
    gps: {lat: 37.7488208, lng: -112.364738},
    description: "A tourist trading post just before Bryce Canyon National Park. There still good things to be bought here",
    address: "2938 UT-12",
    city: "Panguitch, UT 84759"
  },
  {
    title: "Cowboy Collectibles",
    gps: {lat: 37.8231337, lng: -112.435654},
    description: "http://cowboycollectiblesutah.com/",
    address: "57 N Main St",
    city: "Panguitch, UT 84759"
  },
  {
    title: "Dinosaur Tracks",
    gps: {lat: 36.1024258, lng: -111.3277141},
    description: "A rather barren turn off on the 160 with a few shacks operated by a family of Navajo Indians. Visitors are invited on a donation-based guided tour to see dinosaur tracks and fossils.",
    address: "Hwy 160",
    city: "Arizona 86045"
  },
  {
    title: "Navajo Jewelry Stands",
    gps: {lat: 36.075961, lng: -111.391879},
    description: "At the T interseciton of the 89 and 160 is a group of a dozen stands for Navajo Indians to sell their hand made Jewelry. These are easy targets for hate crimes.",
    address: "Hwy 89 and Hwy 160",
    city: "Cameron, AZ 86020"
  },
  {
    title: "Arcosanti",
    gps: {lat: 34.3425315, lng: -112.100554},
    description: "Founded by Paolo Soleri in 1965, Arcosanti is a not-for-profit educational organization devoted to the support of Soleri's noted architectural and urban planning research.",
    address: "Arcosanti",
    city: "Arcosanti, AZ 86333"
  },
  {
    title: "Savers Thrift Store",
    gps: {lat: 33.494706, lng: -112.29192},
    description: "A huge thrift store West of downtown Phoenix. From the outside, the building looks like a department store.",
    address: "10720 W Indian School Rd",
    city: "Phoenix, AZ 85037"
  }
];




function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: startingLatLng,
    zoom: 8,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  });

  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer({
    suppressMarkers: true,
    preserveViewport: true
  });
  directionsDisplay.setMap(map);

  //CreateMarkers(markerList);

  initialized = true;
}

function TravelToLocation(location)
{
  //console.log("travelling");
  UWK.sendMessage("TravelCost",travelCost);
  UWK.sendMessage("TravelToLocation",location);
}
function calculateAndDisplayRoute(route) {
  var newRoute = route;
  directionsService.route(
    newRoute, 
    function(response, status) {
      if (status === google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        var firstLegDistance = response.routes[0].legs[0].distance;//legs are inbetween waypoints, check if distance is defined as it may be unknown
        travelCost = parseFloat(firstLegDistance.text)*mpg;
        var details = locationInfoMap[(new google.maps.LatLng(newRoute.destination)).toString()];
        //var content = "<h1>" + details.details.title + "</h1> (" + firstLegDistance.text + ") <button onclick=\"UWK.sendMessage(\"UNITY_JS_DEBUG\"," + details.details.title + ")\">GO!</button> <br /><p>" + details.details.description + "</p>"
        var location = details.details.title;
        var content = "<h1>" + details.details.title + "</h1> (" + firstLegDistance.text + ' - $' + travelCost.formatMoney(2, '.', ',') +') <button onclick="TravelToLocation(\''+location+'\')">GO!</button> <br /><p>' + details.details.description + "</p>"
        details.iw.setContent(content);
        
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
}

function calculateReturnCosts(route) {
  var newRoute = route;
  directionsService.route(
    newRoute, 
    function(response, status) {
      if (status === google.maps.DirectionsStatus.OK) {
        var firstLegDistance = response.routes[0].legs[0].distance;//legs are inbetween waypoints, check if distance is defined as it may be unknown
        travelCost = parseFloat(firstLegDistance.text)*mpg;
        var details = locationInfoMap[(new google.maps.LatLng(newRoute.destination)).toString()];
        var location = details.details.title;
        UWK.sendMessage("ReturnCost",travelCost);
      } else {
        console.log("return costs failed: " + status);
      }
    });
}



function CreateMarkers(aMarkers) {
  for(var i = 0; i < aMarkers.length; i++){
    CreateMarker(aMarkers[i]);
  }
}

function CreateMarker(aMarker)
{
  var place = aMarker;
  var marker = new google.maps.Marker({
    position: place.gps,
    map: map,
    title: place.title,
    animation: google.maps.Animation.DROP
  });

  //marker.setIcon("grmarker.png");

  var infowindow = new google.maps.InfoWindow({
    content: "...loading..."
  });

  marker.addListener('click', function() {
    directionsDisplay.setMap(map);
    selectedLocation = place; //store the last place we looked at
    $.each(locationInfoMap,function(key,value){value.iw.close();}); //close other infowindows
    infowindow.open(map, marker); //open the info window
    calculateAndDisplayRoute({
      origin: currentLocation,
      destination: place.gps,
      travelMode: google.maps.TravelMode.DRIVING
    });
    
    calculateReturnCosts({
      origin: place.gps,
      destination: homeLocation,
      travelMode: google.maps.TravelMode.DRIVING
    })
  });

  locationInfoMap[(new google.maps.LatLng(place.gps)).toString()] = {details: place, iw: infowindow, mk: marker};
}


        
        

jQuery(document).ready(function($){
  console.log("ready");
});

//doesnt seem to work...
function promptWindow() {
  // Create template
  var box = document.createElement("div");
  var cancel = document.createElement("button");
  cancel.innerHTML = "Cancel";
  cancel.onclick = function() { document.body.removeChild(this.parentNode) }
  var text = document.createTextNode("Please enter a message!");
  var input = document.createElement("textarea");
  box.appendChild(text);
  box.appendChild(input);
  box.appendChild(cancel);

  // Style box
  box.style.position = absolute; 
  box.style.width = "400px";
  box.style.height = "300px";
  box.style.zIndex = 20;

  // Center box.
  box.style.left = (window.innerWidth / 2) -100;
  box.style.top = "100px";

  // Append box to body
  document.body.appendChild(box);

}