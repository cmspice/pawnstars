﻿Shader "Custom/NewShader" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
    }
    SubShader {
        Tags { "RenderType" = "Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Lambert

        sampler2D _MainTex;

        struct Input {
            float2 uv_MainTex;
        };

        void surf (Input IN, inout SurfaceOutput o) {
			int amt = 32;
            half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo.r = (((int)(c.r*256))/amt*amt)/256.0;
			o.Albedo.g = (((int)(c.g*256))/amt*amt)/256.0;
			o.Albedo.b = (((int)(c.b*256))/amt*amt)/256.0;
			o.Alpha = (((int)(c.a*256))/amt*amt)/256.0;
            //o.Albedo = c.rgb;
            //o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}