﻿using UnityEngine;
using System.Collections;

public class MoveAThingUp : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 changeInPosition = Vector3.up * Input.GetAxis ("Vertical") * Time.deltaTime;
		transform.position += changeInPosition;
	}
}
