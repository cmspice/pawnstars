﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Light))]
public class LightCycle : MonoBehaviour
{
	public Gradient colors;
	public float speed;
	float time = 0;
	Light light;
	// Use this for initialization
	void Start () 
	{
		light = gameObject.GetComponent<Light> ();
		light.color = colors.Evaluate (0);
	}
	
	// Update is called once per frame
	void Update ()
	{
		time += Time.deltaTime * speed;
		while (time>1)
			time -= 1;
		light.color = colors.Evaluate (time);
	}
}
