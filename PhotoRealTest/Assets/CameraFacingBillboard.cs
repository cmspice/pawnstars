﻿using UnityEngine;
using System.Collections;

public class CameraFacingBillboard : MonoBehaviour
{
	public Camera m_Camera;
	
	void Update()
	{
		if (m_Camera == null)
			m_Camera = Camera.main;

		Vector3 lookDir = m_Camera.transform.position - transform.position;
		lookDir.y = 0;
		transform.LookAt (transform.position - lookDir.normalized, Vector3.up);
		/*
		Vector3 Off = m_Camera.transform.forward;
		Off.y = 0;
		transform.LookAt(transform.position + Off,
		                Vector3.up);
		                */
	}
}