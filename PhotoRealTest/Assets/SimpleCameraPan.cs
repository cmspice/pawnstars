﻿using UnityEngine;
using System.Collections;

public class SimpleCameraPan : MonoBehaviour 
{
	public float moveSpeed = 0.25f;
	// Use this for initialization
	void Start () {
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}
	
	// Update is called once per frame
	void LateUpdate()
	{
		transform.position += Vector3.right * Time.deltaTime * Input.GetAxis ("Horizontal") * moveSpeed;
		transform.position += Vector3.up * Time.deltaTime * Input.GetAxis ("Vertical") * moveSpeed;
	}
}
