::ITEMS::
-Kanga
	-A stuffed kangaroo. It was loved very much before it was lost. The story of the kangaroo and the story of its owner have long since diverged. 
-Si Bu Xian "wo wo"
	-A non-descript stuffed quadraped. It is white, dirty and old. It was loved very much for a long time before it was discarded due to unfortunatey circumstances.
-Tamogotchi keychain
	-A keychain featuring a Tamagotchi character not to be confused with the Tamagotchi toy itself. These were obtained from McDonald's Happy Meals during the Tamagotchi craze of the 90's. But who even remembers any of this?
-RC Helicopter
	-A cheap RC Helicopter. These were made technologically possible by Lithium Ion Polymer batteries--small, cheap and light. It has two counter rotating propellors to maintain its orientation. Brand new in its box. This model looks as if it were purchased from Amazon and never opened. This looks like it would be quite fun if paired with a toy ballistic device.
-NERF Gun
	-A six barreled revolver that shoots foam darts. These are quite common. If only there was some sort of harmless flying toy that could be used as a target.
-Price sticker gun
	-An industry standard tool used for adhering price labels to products--commonly used in shops around the world where UPS system is unsuitable. This particular device is weathered and has stray bits of tape residue aged onto its handle. A robust tool that must have been in service for many years.
-Ceramic vase
	-A ceramic vase with a figure of a face pressing out of the side. It is glazed in a brilliant gloss black fading to a tan yellow on many of its features. Though it is beautiful, it looks like to be made by an amateur. The vase is signed by two small rectangular indentations that adorn its foot. 

-----------------------

-Soapstone Bear
	-A black bear in a towering pose carved from dark soapstone. Elegant brightly colored streaaks adorn its body. A symbol of nature that can firmly hold down a stack of paper.
-TEST CUBE
	-A perfect cube assembled out of 8 triangles. Developers would use these items as placeholders for the final assets.
-Model Car
	-A model car. The kind you assemble from a kit. It looks like it was carelessly assembled by a little boy.
-Sponge
	-A large synthetic sponge made to look natural. It's cut in the standard "peanut" shape. It was purchased as a house warming gift but was never used.
-Taxidermy Duck
	-A real taxidermy duck. It's quite old and fragile. The duck gives an aura of darkness but its history is unkown.
-Taxidermy Bird
	-An exotic taxidermy bird. It's probably illegal to own.
-Rock
	-An igneous rock. Many years ago this was somebody's pet.
-Plunger
	-A standard home plunger. This one looks brand new.
-Counter
	-An oak laminated shelf. These aren't built to last but this one appears to be in immaculate condition.
-Barrel
	-A french oak barrel. These barrels are made for aging wine and then sold as home objects after industrial use.
-Pigarus	
	-A pig that flew too close to the sun and was cursed to be a plastic desk ornament.
-Victorian Armoire
	-An antique Armoire that might date back to the gold rush, in fair condition.
-Heinous Armchair
	-A standard armchaiar upholstered in a robust carpet textured fabric.

::LOCATIONS::